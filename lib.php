<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die();

use theme_ethink\css_processor;

/**
 * Makes our changes to the CSS
 *
 * This is only called when compiling CSS after cache clearing.
 *
 * @param string $css
 * @param theme_config $theme
 * @return string
 */
function theme_ethink_process_css($css, $theme) {

    $processor   = new css_processor($theme);
    $settingscss = $processor->get_settings_css($css);

    if (empty($theme->settings->enablestyleoverrides)) {
        // Replace all instances ($settingscss is an array).
        $css = str_replace($settingscss, '', $css);
        // Always insert settings-based custom CSS.
        return $processor->replace_tokens(array('customcss' => css_processor::$DEFAULT_CUSTOMCSS), $css);
    }

    $replacements = $settingscss;

    // Based on ethink Bootswatch.
    // These defaults will also be used to generate and replace
    // variant colours (e.g. linkcolor-dark, linkcolor-darker).
    $variantdefaults = array(
        'linkcolor'        => css_processor::$DEFAULT_LINKCOLOR,
        'linkvisitedcolor' => css_processor::$DEFAULT_LINKVISITEDCOLOR,
        'buttoncolor'      => css_processor::$DEFAULT_BUTTONCOLOR,
        'loginboxbackground'      => css_processor::$DEFAULT_LOGINBOXBACKGROUND,
        'footerbackground'      => css_processor::$DEFAULT_FOOTERBACKGROUND,
        'loginpagebackgroundcolor'      => css_processor::$DEFAULT_LOGINPAGEBACKGROUNDCOLOR,
        'footerlinkcolor'      => css_processor::$DEFAULT_FOOTERLINKCOLOR,
        'footericoncolor'      => css_processor::$DEFAULT_FOOTERICONCOLOR,
        'slidertitlecolor'      => css_processor::$DEFAULT_SLIDERTITLECOLOR,
        'slidertextcolor'      => css_processor::$DEFAULT_SLIDERTEXTCOLOR,
        'slidertextbackgroundcolor'      => css_processor::$DEFAULT_SLIDERTEXTBACKGROUNDCOLOR,
        'headingcolor'        => css_processor::$DEFAULT_HEADINGCOLOR,
        'headerbackgroundcolortop'        => css_processor::$DEFAULT_HEADERBACKGROUNDCOLORTOP,
        'headerbackgroundcolormiddle'        => css_processor::$DEFAULT_HEADERBACKGROUNDCOLORMIDDLE,
        'headerbackgroundcolorbottom'        => css_processor::$DEFAULT_HEADERBACKGROUNDCOLORBOTTOM,
        'headerbackgroundcolorexpandlist'        => css_processor::$DEFAULT_HEADERBACKGROUNDCOLOREXPANDLIST,
        'headercolor'        => css_processor::$DEFAULT_HEADERCOLOR,
        'headerdropdowncolor'        => css_processor::$DEFAULT_HEADERDROPDOWNCOLOR,
        'headerdropdowncolorh'        => css_processor::$DEFAULT_HEADERDROPDOWNCOLORH,
        'headeritembackgroundcolora'        => css_processor::$DEFAULT_HEADERITEMBACKGROUNDCOLORA,
        'headeritemcolora'        => css_processor::$DEFAULT_HEADERITEMCOLORA,
        'headeritemcolorh'        => css_processor::$DEFAULT_HEADERITEMCOLORH,
        'headeritembackgroundcolorh'        => css_processor::$DEFAULT_HEADERITEMBACKGROUNDCOLORH,
        'headeradminpanelbackgroundcolor'        => css_processor::$DEFAULT_HEADERADMINPANELBACKGROUNDCOLOR,
        'headeradminpaneltextcolor'        => css_processor::$DEFAULT_HEADERADMINPANELTEXTCOLOR,
        'headeradminpanellinkcolor'        => css_processor::$DEFAULT_HEADERADMINPANELLINKCOLOR,
        'linkhovercolor'        => css_processor::$DEFAULT_LINKHOVERCOLOR,
        'buttontextcolor'      => css_processor::$DEFAULT_BUTTONTEXTCOLOR,
        'buttontextcolorh'      => css_processor::$DEFAULT_BUTTONTEXTCOLORH,
        'buttonbackgroundcolorh'      => css_processor::$DEFAULT_BUTTONBACKGROUNDCOLORH,
        'dbuttoncolor'      => css_processor::$DEFAULT_DBUTTONCOLOR,
        'dbuttontextcolor'      => css_processor::$DEFAULT_DBUTTONTEXTCOLOR,
        'dbuttontextcolorh'      => css_processor::$DEFAULT_DBUTTONTEXTCOLORH,
        'dbuttonbackgroundcolorh'      => css_processor::$DEFAULT_DBUTTONBACKGROUNDCOLORH,

        'headernewsbackgroundcolor'      => css_processor::$DEFAULT_HEADERNEWSBACKGROUNDCOLOR,
        'headernewstextcolor'      => css_processor::$DEFAULT_HEADERNEWSTEXTCOLOR,
        'headernewslinkcolor'      => css_processor::$DEFAULT_HEADERNEWSLINKCOLOR,
        'headernewsunreadbgc'      => css_processor::$DEFAULT_HEADERNEWSUNREADBGC,
        'headernewsunreadbgch'      => css_processor::$DEFAULT_HEADERNEWSUNREADBGCH,
        'headernewsuncountbgc'      => css_processor::$DEFAULT_HEADERNEWSCOUNTBGC,
        'headernewsuncountc'      => css_processor::$DEFAULT_HEADERNEWSCOUNTC,
    );

    // These default values do not have programmatic variants.
    $nonvariantdefaults = array(
        'contentbackground' => css_processor::$DEFAULT_CONTENTBACKGROUND,
        'bodybackground'    => css_processor::$DEFAULT_BODYBACKGROUND,
        'textcolor'         => css_processor::$DEFAULT_TEXTCOLOR,
        'navtextcolor'      => css_processor::$DEFAULT_NAVTEXTCOLOR,
        'sidebarbgc'      => css_processor::$DEFAULT_SIDEBARBGC,
        'sidebarburgerbackgroundc'      => css_processor::$DEFAULT_SIDEBARBURGERBACKGROUNDC,
        'sidebarburgerbackgroundch'      => css_processor::$DEFAULT_SIDEBARBURGERBACKGROUNDCH,
        'sidebarburgerc'      => css_processor::$DEFAULT_SIDEBARBURGERC,
        'sidebarburgerco'      => css_processor::$DEFAULT_SIDEBARBURGERCO,
        'sidebarburgerch'      => css_processor::$DEFAULT_SIDEBARBURGERCH,
        'sidebartextc'      => css_processor::$DEFAULT_SIDEBARTEXTC,
        'sidebaritemch'      => css_processor::$DEFAULT_SIDEBARITEMCH,
        'sidebaritemca'      => css_processor::$DEFAULT_SIDEBARITEMCA,
        'sidebaritemcs'      => css_processor::$DEFAULT_SIDEBARITEMCS,


        'sidebaritemtextch'      => css_processor::$DEFAULT_SIDEBARITEMTEXTCH,
        'sidebaritemtextca'      => css_processor::$DEFAULT_SIDEBARITEMTEXTCA,
        'sidebaritemtextcs'      => css_processor::$DEFAULT_SIDEBARITEMTEXTCS,
    );

    foreach (array_values($replacements) as $i => $replacement) {
        $replacements[$i] = $processor->replace_colours($variantdefaults, $replacement);
        $replacements[$i] = $processor->replace_tokens($nonvariantdefaults, $replacements[$i]);
        $replacements[$i] = $processor->remove_delimiters($replacements[$i]);
    }

    if (!empty($settingscss)) {
        $css = str_replace($settingscss, $replacements, $css);
    }

    // Settings based CSS is not applied conditionally.
    $css = $processor->replace_tokens(array('customcss' => css_processor::$DEFAULT_CUSTOMCSS), $css);

    return $css;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_ethink_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM && (
        $filearea === 'logo' || 
        $filearea === 'favicon' || 
        $filearea === 'backgroundimage' ||
        $filearea === 'loginbg' ||
        $filearea === 'tile1' ||
        $filearea === 'tile2' ||
        $filearea === 'tile3' ||
        $filearea === 'tile4' ||
        $filearea === 'tile5' ||
        $filearea === 'tile6' ||
        $filearea === 'tile7' ||
        $filearea === 'tile8' ||
        $filearea === 'slide1' ||
        $filearea === 'slide2' ||
        $filearea === 'slide3' ||
        $filearea === 'slide4' ||
        $filearea === 'slide5' ||
        $filearea === 'slide6' ||
        $filearea === 'slide7' ||
        $filearea === 'slide8' ||
        $filearea === 'slide9' ||
        $filearea === 'slide10' ||
        $filearea === 'dregionbg1' ||
        $filearea === 'fregionbg1' ||
        $filearea === 'regionbg1' ||
        $filearea === 'dregionbg2' ||
        $filearea === 'fregionbg2' ||
        $filearea === 'regionbg2' ||
        $filearea === 'dregionbg3' ||
        $filearea === 'fregionbg3' ||
        $filearea === 'regionbg3' ||
        $filearea === 'dregionbg4' ||
        $filearea === 'fregionbg4' ||
        $filearea === 'regionbg4' ||
        $filearea === 'dregionbg5' ||
        $filearea === 'fregion5bg' ||
        $filearea === 'regionbg5' ||
        $filearea === 'dregionbg6' ||
        $filearea === 'fregionbg6' ||
        $filearea === 'regionbg6' ||
        $filearea === 'dregionbg7' ||
        $filearea === 'fregionbg7' ||
        $filearea === 'regionbg7' ||
        $filearea === 'dregionbg8' ||
        $filearea === 'fregionbg8' ||
        $filearea === 'regionbg8' ||
        $filearea === 'dregionbg9' ||
        $filearea === 'fregionbg9' ||
        $filearea === 'regionbg9' ||
        $filearea === 'dregionbg10' ||
        $filearea === 'fregionbg10' ||
        $filearea === 'regionbg10' ||
        //BottomsBGs
        $filearea === 'dbregionbg1' ||
        $filearea === 'fbregionbg1' ||
        $filearea === 'bregionbg1' ||
        $filearea === 'dbregionbg2' ||
        $filearea === 'fbregionbg2' ||
        $filearea === 'bregionbg2' ||
        $filearea === 'dbregionbg3' ||
        $filearea === 'fbregionbg3' ||
        $filearea === 'bregionbg3' ||
        $filearea === 'dbregionbg4' ||
        $filearea === 'fbregionbg4' ||
        $filearea === 'bregionbg4' ||
        //GeneralBGs
        $filearea === 'gregionbg1' ||
        $filearea === 'gregionbg2' ||
        $filearea === 'gregionbg3' ||
        $filearea === 'gregionbg4' ||
        $filearea === 'gregionbg5' ||
        $filearea === 'gregionbg6' ||
        $filearea === 'gregionbg7' ||
        $filearea === 'gregionbg8' ||
        $filearea === 'gregionbg9' ||
        $filearea === 'gregionbg10' ||
        $filearea === 'gbregionbg1' ||
        $filearea === 'gbregionbg2' ||
        $filearea === 'gbregionbg3' ||
        $filearea === 'gbregionbg4' ||


        $filearea === 'loginpagebackground' ||
        $filearea === 'loginboxlogo' ||
        $filearea === 'topbutton' ||
        $filearea === 'loaderimage' ||
        $filearea === 'footerbackgroundimage'
        )) {
        $theme = theme_config::load('ethink');
        return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
    }

    send_file_not_found();
}
