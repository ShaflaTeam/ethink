<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';


    // -- regionSHOW

    // $ADMIN->add('themes', new admin_category($component, get_string('ethinksettings', $component)));

    $temp = new admin_settingpage($component . '_settings_fpregions', get_string('fpregions', $component . ''));

    $name = $component . '/regionsamount';
    $title = get_string('fpregionsamount' , $component . '');
    $description = get_string('fpregionsamountdesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => 'Disable Frontpage Regions',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7',
      '8' => '8',
      '9' => '9',
      '10' => '10'

      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $alphas = range('a', 'z');
    if (!empty($PAGE->theme->settings->regionsamount)) {
      if ($PAGE->theme->settings->regionsamount > 0) {
        for ($i = 1; $i <= $PAGE->theme->settings->regionsamount; $i++) {
            
          $name = $component . '/region'.$alphas[$i-1];
          $title = get_string('fpregion'.$i, $component . '');
          $description = get_string('fpregion'.$i.'desc', $component . '');
          $default = '100';
          $choices = array(
            '100' => '100%',
            '75' => '75%',
            '66' => '66%',
            '50' => '50%',
            '33' => '33%',
            '25' => '25%'
            );
          $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

        }
      }
    }



    // BOTTOM REGIONS 

    $name = $component . '/dbottomregionsamountheading';
    $title = get_string('fbottomregionsheading' , $component . '');
    $description = get_string('fbottomregionsamountheading', $component . '');
    $setting = new admin_setting_heading($name, $title, $description, $default, $choices);
    $temp->add($setting);

    $name = $component . '/bregionsamount';
    $title = get_string('fbottomregionsamount' , $component . '');
    $description = get_string('fbottomregionsamountdesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => 'Disable Frontpage Bottom Regions',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $alphas = range('a', 'z');
    if (!empty($PAGE->theme->settings->bregionsamount)) {
      if ($PAGE->theme->settings->bregionsamount > 0) {
        for ($i = 1; $i <= $PAGE->theme->settings->bregionsamount; $i++) {
            
          $name = $component . '/bregion'.$alphas[$i-1];
          $title = get_string('fbregion'.$i, $component . '');
          $description = get_string('fbregion'.$i.'desc', $component . '');
          $default = '100';
          $choices = array(
            '100' => '100%',
            '75' => '75%',
            '66' => '66%',
            '50' => '50%',
            '33' => '33%',
            '25' => '25%'
            );
          $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

              // -- Region bakcgorund image.
          $name = $component . '/bregionbg'.$i;
          $title = get_string('fbregionbg'.$i, $component . '');
          $description = get_string('fbregionbg'.$i.'desc', $component . '');
          $setting = new admin_setting_configstoredfile($name, $title, $description, 'bregionbg'.$i,0 ,['accepted_types' => 'web_image']);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

              // Region background color
          $name = $component . '/bregioncolor'.$i;
          $title = get_string('fbregionbg'.$i, $component . '');
          $description = get_string('fbregionbg'.$i.'desc', $component . '');
          $default = '';
          $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);
        }
      }
    }

    
    $ADMIN->add($component . '', $temp);
