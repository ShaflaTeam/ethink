<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';

if ($ADMIN->fulltree) {

$temp = new admin_settingpage($component . '_settings_search', get_string('search', $component . ''));

    // Enable searchbox 

    $name = "{$component}/searchbox";
    $title = new lang_string('enablesearch', $component);
    $description = new lang_string('enablesearchdesc', $component);
    $default = '1';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = $component . '/searchsource';
    $title = get_string('searchsource' , $component . '');
    $description = get_string('searchsourcedesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => get_string('catalog', $component . ''), 
      '1' => get_string('course', $component . ''),
      '2' => get_string('global', $component . ''),
      '3' => get_string('coursecatalog', $component . '')
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // -- Search position

    $name = $component . '/searchposition';
    $title = get_string('searchposition' , $component . '');
    $description = get_string('searchpositiondesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => get_string('disable', $component . ''), 
      '1' => get_string('topleft', $component . ''),
      '2' => get_string('topcenter', $component . ''),
      '3' => get_string('topright', $component . ''),
      '4' => get_string('navleft', $component . ''),
      '5' => get_string('navright', $component . ''),
      '6' => get_string('botleft', $component . ''),
      '7' => get_string('botcenter', $component . ''),
      '8' => get_string('botright', $component . ''),
      '9' => get_string('sidebar', $component . ''),
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add($component . '', $temp);
}
