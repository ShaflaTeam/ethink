<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';


    $temp = new admin_settingpage($component . '_settings_header', get_string('header', $component . ''));


    // Logo file setting.
    $name = "{$component}/logo";
    $title = new lang_string('logo', 'totara_core');
    $description = new lang_string('logodesc', 'totara_core');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo',0 ,['accepted_types' => 'web_image']);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Logo alt text.
    $name = "{$component}/alttext";
    $title = new lang_string('alttext', 'totara_core');
    $description = new lang_string('alttextdesc', 'totara_core');
    $setting = new admin_setting_configtext($name, $title, $description, '');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Navigation text color.
    // $name = "{$component}/navtextcolor";
    // $title = get_string('navtextcolor', $component);
    // $description = get_string('navtextcolor_desc', $component);
    // $default = css_processor::$DEFAULT_NAVTEXTCOLOR;
    // $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    // $setting->set_updatedcallback('theme_reset_all_caches');
    // $temp->add($setting);


    // -- Menu position

    $name = $component . '/menuposition';
    $title = get_string('menuposition' , $component . '');
    $description = get_string('menupositiondesc', $component . '');
    $default = '2';
    $choices = array(
      '0' => get_string('disable', $component . ''), 
      '1' => get_string('left', $component . ''), 
      '2' => get_string('center', $component . ''), 
      '3' => get_string('right', $component . ''),
      '4' => get_string('sidebar', $component . ''), 
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // -- logo position

    $name = $component . '/logoposition';
    $title = get_string('logoposition' , $component . '');
    $description = get_string('logopositiondesc', $component . '');
    $default = '4';
    $choices = array(
      '0' => get_string('disable', $component . ''), 
      '1' => get_string('topleft', $component . ''),
      '2' => get_string('topcenter', $component . ''),
      '3' => get_string('topright', $component . ''),
      '4' => get_string('navleft', $component . ''),
      '5' => get_string('navright', $component . ''),
      '6' => get_string('botleft', $component . ''),
      '7' => get_string('botcenter', $component . ''),
      '8' => get_string('botright', $component . ''),
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // -- usermenu position

    $name = $component . '/usermenuposition';
    $title = get_string('usermenuposition' , $component . '');
    $description = get_string('usermenupositiondesc', $component . '');
    $default = '5';
    $choices = array(
      '0' => get_string('disable', $component . ''), 
      '1' => get_string('topleft', $component . ''),
      '2' => get_string('topcenter', $component . ''),
      '3' => get_string('topright', $component . ''),
      '4' => get_string('navleft', $component . ''),
      '5' => get_string('navright', $component . ''),
      '6' => get_string('botleft', $component . ''),
      '7' => get_string('botcenter', $component . ''),
      '8' => get_string('botright', $component . ''),
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = "{$component}/showsidebar";
    $title = new lang_string('showsidebar', $component);
    $description = new lang_string('showsidebar_desc', $component);
    $default = '1';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);


    $name = "{$component}/showavatar";
    $title = new lang_string('showavatar', $component);
    $description = new lang_string('showavatar_desc', $component);
    $default = '1';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);


    // Sidebar background colour setting.
    $name = "{$component}/sidebarbgc";
    $title = new lang_string('sidebarbgc', $component);
    $description = new lang_string('sidebarbgcdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARBGC;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar Burger button background colour setting.
    $name = "{$component}/sidebarburgerbackgroundc";
    $title = new lang_string('sidebarburgerbackgroundc', $component);
    $description = new lang_string('sidebarburgerbackgroundcdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARBURGERBACKGROUNDC;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar Burger button background colour hover setting.
    $name = "{$component}/sidebarburgerbackgroundch";
    $title = new lang_string('sidebarburgerbackgroundch', $component);
    $description = new lang_string('sidebarburgerbackgroundchdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARBURGERBACKGROUNDCH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar Burger button colour setting.
    $name = "{$component}/sidebarburgerc";
    $title = new lang_string('sidebarburgerc', $component);
    $description = new lang_string('sidebarburgercdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARBURGERC;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar Burger button hover colour setting.
    $name = "{$component}/sidebarburgerch";
    $title = new lang_string('sidebarburgerch', $component);
    $description = new lang_string('sidebarburgerchdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARBURGERCH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar Burger button open colour setting.
    $name = "{$component}/sidebarburgerco";
    $title = new lang_string('sidebarburgerco', $component);
    $description = new lang_string('sidebarburgercodesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARBURGERCO;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar text colour setting.
    $name = "{$component}/sidebartextc";
    $title = new lang_string('sidebartextc', $component);
    $description = new lang_string('sidebartextcdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARTEXTC;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar element hover colour setting.
    $name = "{$component}/sidebaritemch";
    $title = new lang_string('sidebaritemch', $component);
    $description = new lang_string('sidebaritemchdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARITEMCH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar menu element active colour setting.
    $name = "{$component}/sidebaritemca";
    $title = new lang_string('sidebaritemca', $component);
    $description = new lang_string('sidebaritemcadesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARITEMCA;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar menu element selected colour setting.
    $name = "{$component}/sidebaritemcs";
    $title = new lang_string('sidebaritemcs', $component);
    $description = new lang_string('sidebaritemcsdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARITEMCS;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar element hover text colour setting.
    $name = "{$component}/sidebaritemtextch";
    $title = new lang_string('sidebaritemtextch', $component);
    $description = new lang_string('sidebaritemtextchdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARITEMTEXTCH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar menu element active text colour setting.
    $name = "{$component}/sidebaritemtextca";
    $title = new lang_string('sidebaritemtextca', $component);
    $description = new lang_string('sidebaritemtextcadesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARITEMTEXTCA;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Sidebar menu element selected text colour setting.
    $name = "{$component}/sidebaritemtextcs";
    $title = new lang_string('sidebaritemtextcs', $component);
    $description = new lang_string('sidebaritemtextcsdesc', $component);
    $default = css_processor::$DEFAULT_SIDEBARITEMTEXTCS;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header background colour setting.
    $name = "{$component}/headerbackgroundcolortop";
    $title = new lang_string('headerbackgroundcolortop', $component);
    $description = new lang_string('headerbackgroundcolortopdesc', $component);
    $default = css_processor::$DEFAULT_HEADERBACKGROUNDCOLORTOP;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header background colour setting.
    $name = "{$component}/headerbackgroundcolormiddle";
    $title = new lang_string('headerbackgroundcolormiddle', $component);
    $description = new lang_string('headerbackgroundcolormiddledesc', $component);
    $default = css_processor::$DEFAULT_HEADERBACKGROUNDCOLORMIDDLE;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header background colour setting.
    $name = "{$component}/headerbackgroundcolorbottom";
    $title = new lang_string('headerbackgroundcolorbottom', $component);
    $description = new lang_string('headerbackgroundcolorbottomdesc', $component);
    $default = css_processor::$DEFAULT_HEADERBACKGROUNDCOLORBOTTOM;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header background colour setting.
    $name = "{$component}/headerbackgroundcolorexpandlist";
    $title = new lang_string('headerbackgroundcolorexpandlist', $component);
    $description = new lang_string('headerbackgroundcolorexpandlistdesc', $component);
    $default = css_processor::$DEFAULT_HEADERBACKGROUNDCOLOREXPANDLIST;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header colour setting.
    $name = "{$component}/headercolor";
    $title = new lang_string('headercolor', $component);
    $description = new lang_string('headercolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //navigation active item background color
    $name = "{$component}/headeritembackgroundcolora";
    $title = new lang_string('headeritembackgroundcolora', $component);
    $description = new lang_string('headeritembackgroundcoloradesc', $component);
    $default = css_processor::$DEFAULT_HEADERITEMBACKGROUNDCOLORA;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //navigation active item color
    $name = "{$component}/headeritemcolora";
    $title = new lang_string('headeritemcolora', $component);
    $description = new lang_string('headeritemcoloradesc', $component);
    $default = css_processor::$DEFAULT_HEADERITEMCOLORA;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //navigation hover item color
    $name = "{$component}/headeritemcolorh";
    $title = new lang_string('headeritemcolorh', $component);
    $description = new lang_string('headeritemcolorhdesc', $component);
    $default = css_processor::$DEFAULT_HEADERITEMCOLORH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //navigation hover item background color
    $name = "{$component}/headeritembackgroundcolorh";
    $title = new lang_string('headeritembackgroundcolorh', $component);
    $description = new lang_string('headeritembackgroundcolorhdesc', $component);
    $default = css_processor::$DEFAULT_HEADERITEMBACKGROUNDCOLORH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel background color
    $name = "{$component}/headeradminpanelbackgroundcolor";
    $title = new lang_string('headeradminpanelbackgroundcolor', $component);
    $description = new lang_string('headeradminpanelbackgroundcolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERADMINPANELBACKGROUNDCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel text color
    $name = "{$component}/headeradminpaneltextcolor";
    $title = new lang_string('headeradminpaneltextcolor', $component);
    $description = new lang_string('headeradminpaneltextcolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERADMINPANELTEXTCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel link color
    $name = "{$component}/headeradminpanellinkcolor";
    $title = new lang_string('headeradminpanellinkcolor', $component);
    $description = new lang_string('headeradminpanellinkcolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERADMINPANELLINKCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header colour setting.
    $name = "{$component}/headerdropdowncolor";
    $title = new lang_string('headerdropdowncolor', $component);
    $description = new lang_string('headerdropdowncolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERDROPDOWNCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Header colour setting.
    $name = "{$component}/headerdropdowncolorh";
    $title = new lang_string('headerdropdowncolorh', $component);
    $description = new lang_string('headerdropdowncolorhdesc', $component);
    $default = css_processor::$DEFAULT_HEADERDROPDOWNCOLORH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);










    //admin panel background color
    $name = "{$component}/headernewsbackgroundcolor";
    $title = new lang_string('headernewsbackgroundcolor', $component);
    $description = new lang_string('headernewsbackgroundcolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERNEWSBACKGROUNDCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel text color
    $name = "{$component}/headernewstextcolor";
    $title = new lang_string('headernewstextcolor', $component);
    $description = new lang_string('headernewstextcolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERNEWSTEXTCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel link color
    $name = "{$component}/headernewslinkcolor";
    $title = new lang_string('headernewslinkcolor', $component);
    $description = new lang_string('headernewslinkcolordesc', $component);
    $default = css_processor::$DEFAULT_HEADERNEWSLINKCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel link color
    $name = "{$component}/headernewsunreadbgc";
    $title = new lang_string('headernewsunreadbgc', $component);
    $description = new lang_string('headernewsunreadbgcdesc', $component);
    $default = css_processor::$DEFAULT_HEADERNEWSUNREADBGC;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel link color
    $name = "{$component}/headernewsunreadbgch";
    $title = new lang_string('headernewsunreadbgch', $component);
    $description = new lang_string('headernewsunreadbgchdesc', $component);
    $default = css_processor::$DEFAULT_HEADERNEWSUNREADBGCH;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel link color
    $name = "{$component}/headernewsuncountbgc";
    $title = new lang_string('headernewsuncountbgc', $component);
    $description = new lang_string('headernewsuncountbgcdesc', $component);
    $default = css_processor::$DEFAULT_HEADERNEWSCOUNTBGC;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    //admin panel link color
    $name = "{$component}/headernewsuncountc";
    $title = new lang_string('headernewsuncountc', $component);
    $description = new lang_string('headernewsuncountcdesc', $component);
    $default = css_processor::$DEFAULT_HEADERNEWSCOUNTC;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add($component, $temp);


