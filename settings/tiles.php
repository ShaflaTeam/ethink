<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';


    // -- tileSHOW

    // $ADMIN->add('themes', new admin_category($component, get_string('ethinksettings', $component)));

    $temp = new admin_settingpage($component . '_settings_tiles', get_string('tiles', $component . ''));

    $name = $component . '/tilesamount';
    $title = get_string('tilesamount' , $component . '');
    $description = get_string('tilesamountdesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => 'Disable Spots',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7',
      '8' => '8'
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = $component . '/tileareatitle';
    $title = get_string('tileareatitle', $component . '');
    $description = get_string('tileareatitledesc', $component . '');
    $default = get_string('marketingregion', $component . '');
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    if (!empty($PAGE->theme->settings->tilesamount)) {
      if ($PAGE->theme->settings->tilesamount > 0) {
        for ($i = 1; $i <= $PAGE->theme->settings->tilesamount; $i++) {
                
          $name = $component . '/tile'.$i;
          $title = get_string('tile'.$i, $component . '');
          $description = get_string('tile'.$i.'desc', $component . '');
          $setting = new admin_setting_configstoredfile($name, $title, $description, 'tile'.$i);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

          $name = $component . '/tile'.$i.'title';
          $title = get_string('tile'.$i.'title', $component . '');
          $description = get_string('tile'.$i.'titledesc', $component . '');
          $default = '';
          $setting = new admin_setting_configtext($name, $title, $description, $default);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

          $name = $component . '/tile'.$i.'text';
          $title = get_string('tile'.$i.'text', $component . '');
          $description = get_string('tile'.$i.'textdesc', $component . '');
          $default = '';
          $setting = new admin_setting_configtext($name, $title, $description, $default);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

          $name = $component . '/tile'.$i.'link';
          $title = get_string('tile'.$i.'link', $component . '');
          $description = get_string('tile'.$i.'linkdesc', $component . '');
          $default = '';
          $setting = new admin_setting_configtext($name, $title, $description, $default);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

        }
      }
    }

    $ADMIN->add($component . '', $temp);


