<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';



    $temp = new admin_settingpage($component . '_settings_socialicons', get_string('socialicons', $component . ''));

    $settings = array (
    'facebook',
    'youtube',
    'linkedin',    
    'twitter',
    'instagram',
    'pinterest',
    'flickr',
    'skype',
    'googleplus',
);

foreach ($settings as $socialnetwork) {
    $setting = $socialnetwork;
    $name = $component  . '/' . $setting;
    $title = get_string($setting, $component);
    $description = get_string($setting . 'desc', $component);
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
}

$ADMIN->add($component . '', $temp);


