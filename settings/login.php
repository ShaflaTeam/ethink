<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';

if ($ADMIN->fulltree) {

    // $ADMIN->add('themes', new admin_category($component, get_string('ethinksettings', $component)));

    $temp = new admin_settingpage($component . '_settings_login', get_string('loginpage', $component . ''));

    // -- loginbox position

    $name = $component . '/loginboxposition';
    $title = get_string('loginboxposition' , $component . '');
    $description = get_string('loginboxpositiondesc', $component . '');
    $default = 'right';
    $choices = array(
      'right' => 'Right',
      'left' => 'Left',
      'center' => 'Center',
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // -- loginpage bakcgorund image.
    $name = "{$component}/loginpagebackground";
    $title = new lang_string('loginpagebackground', $component);
    $description = new lang_string('loginpagebackgrounddesc', $component);
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'loginpagebackground',0 ,['accepted_types' => 'web_image']);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // -- loginpage bakcgorund color.
    $name = "{$component}/loginpagebackgroundcolor";
    $title = get_string('loginpagebackgroundcolor', $component);
    $description = get_string('loginpagebackgroundcolordesc', $component);
    $default = css_processor::$DEFAULT_LOGINPAGEBACKGROUNDCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    // Logo file setting.
    $name = "{$component}/loginboxlogo";
    $title = new lang_string('loginboxlogo', $component);
    $description = new lang_string('loginboxlogodesc', $component);
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'loginboxlogo',0 ,['accepted_types' => 'web_image']);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Loginbox background color
    $name = "{$component}/loginboxbackground";
    $title = get_string('loginboxbackground', $component);
    $description = get_string('loginboxbackground_desc', $component);
    $default = css_processor::$DEFAULT_LOGINBOXBACKGROUND;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Enable / disable non full height loginbox.
    $name = "{$component}/enablenofullheightloginbox";
    $title = new lang_string('enablenofullheightloginbox', $component);
    $description = new lang_string('enablenofullheightloginboxdesc', $component);
    $default = '0';
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add($component . '', $temp);

}
