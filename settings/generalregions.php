<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';

if ($ADMIN->fulltree) {

    // -- regionSHOW

    // $ADMIN->add('themes', new admin_category($component, get_string('ethinksettings', $component)));

    $temp = new admin_settingpage($component . '_settings_gregions', get_string('gregions', $component . ''));

    $name = $component . '/gregionsamount';
    $title = get_string('gregionsamount' , $component . '');
    $description = get_string('gregionsamountdesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => 'Disable General Regions',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7',
      '8' => '8',
      '9' => '9',
      '10' => '10'

      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $alphas = range('a', 'z');
    if (!empty($PAGE->theme->settings->gregionsamount)) {
      if ($PAGE->theme->settings->gregionsamount > 0) {
        for ($i = 1; $i <= $PAGE->theme->settings->gregionsamount; $i++) {
            
          $name = $component . '/gregion'.$alphas[$i-1];
          $title = get_string('gregion'.$i, $component . '');
          $description = get_string('gregion'.$i.'desc', $component . '');
          $default = '100';
          $choices = array(
            '100' => '100%',
            '75' => '75%',
            '66' => '66%',
            '50' => '50%',
            '33' => '33%',
            '25' => '25%'
            );
          $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

              // -- Region bakcgorund image.
          $name = $component . '/gregionbg'.$i;
          $title = get_string('gregionbg'.$i, $component . '');
          $description = get_string('gregionbg'.$i.'desc', $component . '');
          $setting = new admin_setting_configstoredfile($name, $title, $description, 'gregionbg'.$i,0 ,['accepted_types' => 'web_image']);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

              // Region background color
          $name = $component . '/gregioncolor'.$i;
          $title = get_string('gregioncolor'.$i, $component . '');
          $description = get_string('gregioncolor'.$i.'desc', $component . '');
          $default = '';
          $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);
        }
      }
    }


    $name = $component . '/gbottomregionsamountheading';
    $title = get_string('gbottomregionsheading' , $component . '');
    $description = get_string('gbottomregionsamountheading', $component . '');
    $setting = new admin_setting_heading($name, $title, $description, $default, $choices);
    $temp->add($setting);

    $name = $component . '/gbregionsamount';
    $title = get_string('gbottomregionsamount' , $component . '');
    $description = get_string('gbottomregionsamountdesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => 'Disable Bottom Regions',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $alphas = range('a', 'z');
    if (!empty($PAGE->theme->settings->gbregionsamount)) {
      if ($PAGE->theme->settings->gbregionsamount > 0) {
        for ($i = 1; $i <= $PAGE->theme->settings->gbregionsamount; $i++) {
            
          $name = $component . '/gbregion'.$alphas[$i-1];
          $title = get_string('gbregion'.$i, $component . '');
          $description = get_string('gbregion'.$i.'desc', $component . '');
          $default = '100';
          $choices = array(
            '100' => '100%',
            '75' => '75%',
            '66' => '66%',
            '50' => '50%',
            '33' => '33%',
            '25' => '25%'
            );
          $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

              // -- Region bakcgorund image.
          $name = $component . '/gbregionbg'.$i;
          $title = get_string('gbregionbg'.$i, $component . '');
          $description = get_string('gbregionbg'.$i.'desc', $component . '');
          $setting = new admin_setting_configstoredfile($name, $title, $description, 'gbregionbg'.$i,0 ,['accepted_types' => 'web_image']);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

              // Region background color
          $name = $component . '/gbregioncolor'.$i;
          $title = get_string('gbregioncolor'.$i, $component . '');
          $description = get_string('gbregioncolor'.$i.'desc', $component . '');
          $default = '';
          $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, null, false);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);
        }
      }
    }

    $ADMIN->add($component . '', $temp);

}
