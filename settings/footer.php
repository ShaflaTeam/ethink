<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

$component = 'theme_ethink';

     $temp = new admin_settingpage($component . '_settings_footerregions', get_string('footer', $component . ''));


        // Footnote setting.
    $name = "{$component}/footnote";
    $title = get_string('footnote', $component);
    $description = get_string('footnotedesc', $component);
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    

    $name = $component . '/footerregionsamount';
    $title = get_string('footerregionsamount' , $component . '');
    $description = get_string('footerregionsamountdesc', $component . '');
    $default = '0';
    $choices = array(
      '0' => 'Disable Footer Regions',
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4'
      );
    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $alphas = range('a', 'z');
    if (!empty($PAGE->theme->settings->footerregionsamount)) {
      if ($PAGE->theme->settings->footerregionsamount > 0) {
        for ($i = 1; $i <= $PAGE->theme->settings->footerregionsamount; $i++) {
            
          $name = $component . '/footerregion'.$alphas[$i-1];
          $title = get_string('footerregion'.$i, $component . '');
          $description = get_string('footerregion'.$i.'desc', $component . '');
          $default = '100';
          $choices = array(
            '100' => '100%',
            '75' => '75%',
            '66' => '66%',
            '50' => '50%',
            '33' => '33%',
            '25' => '25%'
            );
          $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
          $setting->set_updatedcallback('theme_reset_all_caches');
          $temp->add($setting);

        }
      }
    }

    // Footer background color
    $name = "{$component}/footerbackground";
    $title = get_string('footerbackground', $component);
    $description = get_string('footerbackground_desc', $component);
    $default = css_processor::$DEFAULT_FOOTERBACKGROUND;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // -- Footer background image.
    $name = "{$component}/footerbackgroundimage";
    $title = new lang_string('footerbackgroundimage', $component);
    $description = new lang_string('footerbackgroundimagedesc', $component);
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'footerbackgroundimage',0 ,['accepted_types' => 'web_image']);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Footer link color
    $name = "{$component}/footerlinkcolor";
    $title = get_string('footerlinkcolor', $component);
    $description = get_string('footerlinkcolordesc', $component);
    $default = css_processor::$DEFAULT_FOOTERLINKCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    // Footer icon color
    $name = "{$component}/footericoncolor";
    $title = get_string('footericoncolor', $component);
    $description = get_string('footericoncolordesc', $component);
    $default = css_processor::$DEFAULT_FOOTERICONCOLOR;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add($component . '', $temp);

