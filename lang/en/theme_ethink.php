<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author    Joby Harding <joby.harding@totaralearning.com>
 * @package   theme_ethink
 */
$alphas = range('a', 'z');
$string['choosereadme'] = 'ethink default theme for Totara.';

// -- Position Items

$string['disable'] = 'Disable';
$string['left'] = 'Left';
$string['center'] = 'Center';
$string['right'] = 'Right';
$string['sidebar'] = 'Sidebar';
$string['topleft'] = 'Above Navigation Left';
$string['topcenter'] = 'Above Navigation Center';
$string['topright'] = 'Above Navigation Right';
$string['navleft'] = 'Inline with Navigation Left';
$string['navright'] = 'Inline with Navigation Right';
$string['botleft'] = 'Below Navigation Left';
$string['botcenter'] = 'Below Navigation Center';
$string['botright'] = 'Below Navigation Right';

// -- General

$string['general'] = 'General';
$string['favicon'] = 'Favicon';
$string['favicondesc'] = 'Select or upload the image file to be used as the site\'s favicon, the icon must be *.ico format';
$string['enablestyleoverrides'] = 'Enable style overrides';
$string['enablestyleoverrides_desc'] = 'Allow theme colour settings to override theme defaults.';
$string['textcolor'] = 'Text colour';
$string['textcolor_desc'] = 'The colour of the general text on the site';
$string['linkcolor'] = 'Link colour';
$string['linkcolordesc'] = 'The default colour of links within the site';
$string['linkvisitedcolor'] = 'Visited link colour';
$string['linkvisitedcolordesc'] = 'The default colour of visited links';
$string['headingcolor'] = 'The default h1,h2,h3 etc colour';
$string['buttoncolor'] = 'Button background colour';
$string['buttoncolordesc'] = 'This sets the colour for the background of buttons.';
$string['buttonbackgroundcolorh'] = 'Button hover background colour';
$string['buttonbackgroundcolorhdesc'] = 'The colour of the button background on hover';
$string['dbuttoncolor'] = 'Disabled button background colour';
$string['dbuttoncolordesc'] = 'This sets the colour for the background of disabled buttons.';
$string['dbuttonbackgroundcolorh'] = 'Disabled button hover background colour';
$string['dbuttonbackgroundcolorhdesc'] = 'The colour of the disabled button background on hover';
$string['dbuttontextcolor'] = 'Disable button text colour';
$string['dbuttontextcolordesc'] = 'The colour of the disable button text';
$string['dbuttontextcolorh'] = 'Disable button text hover colour';
$string['dbuttontextcolorhdesc'] = 'The colour of the disable button text on hover';
/* missing the font colour for buttons */
$string['buttontextcolorh'] = 'Button text hover colour';
$string['buttontextcolorhdesc'] = 'The colour of the button text on hover';
$string['bodybackground'] = 'Background colour';
$string['bodybackground_desc'] = 'The main colour to use for the background.';
$string['contentbackground'] = 'Main content background colour';
$string['contentbackground_desc'] = 'The background colour of the main content of the site.';
$string['topbutton'] = 'Back to Top image';
$string['topbuttondesc'] = 'Image which is used for showing users to go back to top of screen';
$string['loaderimage'] = 'Loader image';
$string['loaderimagedesc'] = 'Upload the image file to be used as loader. <br>You can create loader from any image using https://loading.io/ website <br> 
Just place an image you would like to animate, and download it as a .gif file';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Any CSS you enter here will be added to every page allowing you to easily customise this theme.';

// -- Header

$string['header'] = 'Header';
$string['logo'] = 'Logo';
$string['logodesc'] = 'Select or upload the image file to be used as the site\'s logo';
$string['alttext'] = 'Logo alternative text';
$string['alttextdesc'] = 'The text that is displayed when an image can\'t be. This text is also read by a screen reader.';
$string['menuposition'] = 'Navigation Position';
$string['menupositiondesc'] = 'Position of the navigation on screen, for mobile navigation will move to side bar';
$string['logoposition'] = 'Logo Position';
$string['logopositiondesc'] = 'Logo\'s position in relation to the Navigation';
$string['usermenuposition'] = 'User Menu Position (Avatar)';
$string['usermenupositiondesc'] = 'Position of the user menu in relation to the Navigation';
$string['showsidebar'] = 'Enable Sidebar';
$string['showsidebar_desc'] = 'Enables the addition of a off screen side bar with block region';
$string['showavatar'] = 'Enable Avatar in Sidebar';
$string['showavatar_desc'] = 'Shows the avatar of the user at the top of the sidebar';
$string['sidebarbgc'] = 'Sidebar background colour';
$string['sidebarbgcdesc'] = 'This sets the sidebar background colour for the theme.';
$string['sidebarburgerbackgroundc'] = 'Sidebar burger background colour';
$string['sidebarburgerbackgroundcdesc'] = 'Sets the background colour of the burger menu';
$string['sidebarburgerbackgroundch'] = 'Sidebar burger background hover colour';
$string['sidebarburgerbackgroundchdesc'] = 'Sets the background colour of the burger menu on hover';
$string['sidebarburgerc'] = 'Sidebar burger colour';
$string['sidebarburgercdesc'] = 'Sets the colour of the burger if enabled';
$string['sidebarburgerch'] = 'Sidebar burger hover colour';
$string['sidebarburgerchdesc'] = 'Sets the hover colour of the burger if enabled';
$string['sidebarburgerco'] = 'Sidebar (Open) burger colour';
$string['sidebarburgercodesc'] = 'Colour of the burger icon when the sidebar is open';
$string['sidebartextc'] = 'Sidebar text colour';
$string['sidebartextcdesc'] = 'Colour of the text on the sidebar';
$string['sidebaritemch'] = 'Sidebar navigation item hover background colour';
$string['sidebaritemchdesc'] = 'Colour of navigation item background when hovered';
$string['sidebaritemca'] = 'Sidebar navigation item active background colour';
$string['sidebaritemcadesc'] = 'Colour of navigation item background when active';
$string['sidebaritemcs'] = 'Sidebar selected menu element background colour';
$string['sidebaritemcsdesc'] = 'Default colour of the selected menu element background';
$string['sidebaritemtextch'] = 'Sidebar navigation item hover text colour';
$string['sidebaritemchtextdesc'] = 'Colour of navigation item text when hovered';
$string['sidebaritemtextca'] = 'Sidebar navigation item active text colour';
$string['sidebaritemtextcadesc'] = 'Colour of navigation item text when active';
$string['sidebaritemtextcs'] = 'Sidebar selected menu element text colour';
$string['sidebaritemtextcsdesc'] = 'Default colour of the selected menu element text';
$string['headerbackgroundcolortop'] = 'Header background colour (top)';
$string['headerbackgroundcolortopdesc'] = 'Default background colour of the header';
$string['headerbackgroundcolormiddle'] = 'Header Background colour (Navigation section)';
$string['headerbackgroundcolormiddledesc'] = 'Background colour of the navigation area in the header';
$string['headerbackgroundcolorbottom'] = 'Header background colour (bottom)';
$string['headerbackgroundcolorbottomdesc'] = 'Default colour of the background within the header under the navigation';
$string['headerbackgroundcolorexpandlist'] = 'Navigation drop down background colour';
$string['headerbackgroundcolorexpandlistdesc'] = 'Background colour of the navigation drop down menu items';
$string['headercolor'] = 'Navigation text colour';
$string['headercolordesc'] = 'Default colour of the links in header area';
$string['headerdropdowncolor'] = 'Navigation drop down menu text colour';
$string['headerdropdowncolordesc'] = 'Default colour of the links in drop down menu';
$string['headerdropdowncolorh'] = 'Navigation drop down menu text colour hover';
$string['headerdropdowncolorhdesc'] = 'Default hover colour of the links in drop down menu';
$string['headeritembackgroundcolora'] = 'Navigation active background colour';
$string['headeritembackgroundcoloradesc'] = 'Background colour of the active navigation item';
$string['headeritemcolora'] = 'Navigation active text colour';
$string['headeritemcoloradesc'] = 'Colour of the text for active navigation item';
$string['headeritemcolorh'] = 'Navigation text colour on hover';
$string['headeritemcolorhdesc'] = 'Colour of the navigation items when on hover';
$string['headeritembackgroundcolorh'] = 'Navigation hover background colour';
$string['headeritembackgroundcolorhdesc'] = 'Background colour of the navigation items on hover';
$string['headeradminpanelbackgroundcolor'] = 'Admin panel background colour';
$string['headeradminpanelbackgroundcolordesc'] = 'Background Colour of the admin panel';
$string['headeradminpaneltextcolor'] = 'Admin panel text colour';
$string['headeradminpaneltextcolordesc'] = 'Colour of the text on the admin panel';
$string['headeradminpanellinkcolor'] = 'Admin panel link colour';
$string['headeradminpanellinkcolordesc'] = 'Colour of the links within the admin panel';


$string['headernewsbackgroundcolor'] = 'News background colour';
$string['headernewsbackgroundcolordesc'] = 'Background colour of the news';
$string['headernewstextcolor'] = 'News text colour';
$string['headernewstextcolordesc'] = 'Colour of the text on the news';
$string['headernewslinkcolor'] = 'News link colour';
$string['headernewslinkcolordesc'] = 'Colour of the links within the news';
$string['headernewsunreadbgc'] = 'News unread background colour';
$string['headernewsunreadbgcdesc'] = 'News unread background colour';
$string['headernewsunreadbgch'] = 'News unread background colour hover';
$string['headernewsunreadbgchdesc'] = 'News unread background colour hover';
$string['headernewsuncountbgc'] = 'News count background colour';
$string['headernewsuncountbgcdesc'] = 'News count background colour';
$string['headernewsuncountc'] = 'News count text colour';
$string['headernewsuncountcdesc'] = 'News count text colour';

// -- General Regions

$string['gregions'] = 'General Regions';
$string['gregionsamount'] = 'Amount of General Regions';
$string['gregionsamountdesc'] = 'Number of regions at the general of every page';

for ($i=1; $i < 21 ; $i++) {
	$string['gregion' . $i]='General Region '. $i  . ' Width' ;
	$string['gregion' . $i.'desc']='Width of general region '. $i  . '. Regions will loop onto two rows if over 100% in total.' ;
	$string['region-gregion' . $alphas[$i-1].'']='Custom General Region '. $i  . '' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['gregionbg' . $i]='General Region '. $i  . ' Background Image' ;
	$string['gregionbg' . $i.'desc']='General Region '. $i  . ' Background Image' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['gregioncolor' . $i]='General Region '. $i  . ' Background Colour' ;
	$string['gregioncolor' . $i.'desc']='General Region '. $i  . ' Background Colour' ;
}

$string['gbottomregionsheading'] = 'Bottom Regions';
$string['gbottomregionsamount'] = 'Amount of Bottom Regions';
$string['gbottomregionsamountdesc'] = 'Number of regions at the bottom of every page';

for ($i=1; $i < 21 ; $i++) {
	$string['gbregion' . $i]='Bottom Region '. $i  . ' Width' ;
	$string['gbregion' . $i.'desc']='Width of bottom region '. $i  . '. Regions will loop onto two rows if over 100% in total.' ;
	$string['region-gbregion' . $alphas[$i-1].'']='Custom Bottom Region '. $i  . '' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['gbregionbg' . $i]='Bottom Region '. $i  . ' Background Image' ;
	$string['gbregionbg' . $i.'desc']='Bottom Region '. $i  . ' Background Image' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['gbregioncolor' . $i]='Bottom Region '. $i  . ' Background Colour' ;
	$string['gbregioncolor' . $i.'desc']='Bottom Region '. $i  . ' Background Colour' ;
}

$string['gbottomregionsamountheading']='Bottom Regions';

// -- Front Page Regions

$string['fpregions'] = 'Frontpage Regions';
$string['fpregionsamount'] = 'Amount of Frontpage Regions';
$string['fpregionsamountdesc'] = 'Number of regions at the frontpage of every page';

for ($i=1; $i < 21 ; $i++) {
	$string['fpregion' . $i]='Frontpage Region '. $i  . ' Width' ;
	$string['fpregion' . $i.'desc']='Width of frontpage region '. $i  . '. Regions will loop onto two rows if over 100% in total.' ;
	$string['region-fpregion' . $alphas[$i-1].'']='Custom Frontpage Region '. $i  . '' ;
}

$string['fbottomregionsheading'] = 'Frontpage Bottom Regions';
$string['fbottomregionsamountheading'] = 'Frontpage Bottom Regions';
$string['fbottomregionsamount'] = 'Amount of Frontpage Bottom Regions';
$string['fbottomregionsamountdesc'] = 'Number of regions at the frontpage bottom of every page';

for ($i=1; $i < 21 ; $i++) {
	$string['fbregion' . $i]='Frontpage Bottom Region '. $i  . ' Width' ;
	$string['fbregion' . $i.'desc']='Frontpage Width of bottom region '. $i  . '. Regions will loop onto two rows if over 100% in total.' ;
	$string['region-fbregion' . $alphas[$i-1].'']='Custom Frontpage Bottom Region '. $i  . '' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['fbregionbg' . $i]='Frontpage Bottom Region '. $i  . ' Background Image' ;
	$string['fbregionbg' . $i.'desc']='Frontpage Bottom Region '. $i  . ' Background Image' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['fbregioncolor' . $i]='Frontpage Bottom Region '. $i  . ' Background Colour' ;
	$string['fbregioncolor' . $i.'desc']='Frontpage Bottom Region '. $i  . ' Background Colour' ;
}

// -- Dashboard Regions

$string['dregions'] = 'Dashboard Regions';
$string['dregionsamount'] = 'Amount of Dashboard Regions';
$string['dregionsamountdesc'] = 'Number of regions at the dashboard of every page';

for ($i=1; $i < 21 ; $i++) {
	$string['dregion' . $i]='Dashboard Region '. $i  . ' Width' ;
	$string['dregion' . $i.'desc']='Width of dashboard region '. $i  . '. Regions will loop onto two rows if over 100% in total.' ;
	$string['region-dregion' . $alphas[$i-1].'']='Custom Dashboard Region '. $i  . '' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['dregionbg' . $i]='Dashboard Region '. $i  . ' Background Image' ;
	$string['dregionbg' . $i.'desc']='General Region '. $i  . ' Background Image' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['dregioncolor' . $i]='General Region '. $i  . ' Background Colour' ;
	$string['dregioncolor' . $i.'desc']='General Region '. $i  . ' Background Colour' ;
}

$string['dbottomregionsheading'] = 'Dashboard Bottom Regions';
$string['dbottomregionsamountheading'] = 'Dashboard Bottom Regions';
$string['dbottomregionsamount'] = 'Amount of Dashboard Bottom Regions';
$string['dbottomregionsamountdesc'] = 'Number of regions at the dashboard bottom of every page';

for ($i=1; $i < 21 ; $i++) {
	$string['dbregion' . $i]='Dashboard Bottom Region '. $i  . ' Width' ;
	$string['dbregion' . $i.'desc']='Dashboard Width of bottom region '. $i  . '. Regions will loop onto two rows if over 100% in total.' ;
	$string['region-dbregion' . $alphas[$i-1].'']='Custom Bottom Region '. $i  . '' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['dbregionbg' . $i]='Dashboard Bottom Region '. $i  . ' Background Image' ;
	$string['dbregionbg' . $i.'desc']='Dashboard Bottom Region '. $i  . ' Background Image' ;
}
for ($i=1; $i < 21 ; $i++) {
	$string['dbregioncolor' . $i]='Dashboard Bottom Region '. $i  . ' Background Colour' ;
	$string['dbregioncolor' . $i.'desc']='Dashboard Bottom Region '. $i  . ' Background Colour' ;
}


// -- Search

$string['search'] = 'Search Options';
$string['enablesearch'] = 'Enable Search box in Header';
$string['enablesearchdesc'] = 'Enables the search box in the header';
$string['searchsource'] = 'Search Source';
$string['searchsourcedesc'] = 'Select source where the user can search from';
$string['searchposition'] = 'Search box position';
$string['searchpositiondesc'] = 'Position of where the search box should be placed';

$string['catalog'] = 'Catalog Search';
$string['course'] = 'Course Search';
$string['global'] = 'Global Search';
$string['coursecatalog'] = 'Course Catalog (Find Learning) Search';


// -- Slideshow

$string['slideshow'] = 'Slideshow';

$string['slidesamount'] = 'Amount of slides';
$string['slidesamountdesc'] = 'Amount of slides within the slideshow';

for ($i=1; $i < 11 ; $i++) {
	$string['slide'.$i] = 'Slide Background Image';
	$string['slide'.$i.'desc'] = 'Add an image to your slideshow, please make sure to make your pictures are the same size for better visual effect';

	$string['slide'.$i.'title'] = 'Slide Title';
	$string['slide'.$i.'titledesc'] = 'Title of the slide will display as a heading';

	$string['slide'.$i.'text'] = 'Slide Text';
	$string['slide'.$i.'textdesc'] = 'Text of the side will display as smaller text';

	$string['slide'.$i.'link'] = 'Slide Link';
	$string['slide'.$i.'linkdesc'] = 'Link to another page internally or externally';

	$string['slide'.$i.'button'] = 'Slide Button Text';
	$string['slide'.$i.'buttondesc'] = 'Text to be displayed on the button itself';
}

// -- Login page

$string['login_welcome'] = 'Please log in to your e-learning portal';
$string['login_username'] = 'username';
$string['login_password'] = 'password';

$string['loginpage'] = 'Login Page';
$string['loginboxposition'] = 'Login box position';
$string['loginboxpositiondesc'] = 'Position on the page for the login box';
$string['loginpagebackground'] = 'Login page background image';
$string['loginpagebackgrounddesc'] = 'Select or upload an image file to be used as the login page background';
$string['loginpagebackgroundcolor'] = 'Login page background colour';
$string['loginpagebackgroundcolordesc'] = 'Background colour of the login page if no image is selected';
$string['loginboxlogo'] = 'Login page logo';
$string['loginboxlogodesc'] = 'Upload an image file to be used as login page logo.';
$string['loginboxbackground'] = 'Login box background colour';
$string['loginboxbackground_desc'] = 'Background colour of the login box section';
$string['enablenofullheightloginbox'] = 'Display login as a box instead of full height';
$string['enablenofullheightloginboxdesc'] = 'If enabled the login box will appear as a box instead of full height';

// -- Social Icons

$string['socialicons'] = 'Social Icons';
$string['facebook'] = 'Facebook Link';
$string['facebookdesc'] = 'Link to social media, if left blank will not display';
$string['youtube'] = 'Youtube Link';
$string['youtubedesc'] = 'Link to social media, if left blank will not display';
$string['linkedin'] = 'Linkedin Link';
$string['linkedindesc'] = 'Link to social media, if left blank will not display';
$string['twitter'] = 'Twitter Link';
$string['twitterdesc'] = 'Link to social media, if left blank will not display';
$string['instagram'] = 'Instagram Link';
$string['instagramdesc'] = 'Link to social media, if left blank will not display';
$string['pinterest'] = 'Pinterest Link';
$string['pinterestdesc'] = 'Link to social media, if left blank will not display';
$string['flickr'] = 'Flickr Link';
$string['flickrdesc'] = 'Link to social media, if left blank will not display';
$string['skype'] = 'Skype Link';
$string['skypedesc'] = 'Link to social media, if left blank will not display';
$string['googleplus'] = 'Googleplus Link';
$string['googleplusdesc'] = 'Link to social media, if left blank will not display';

// -- Marketing Spots

$string['tiles'] = 'Marketing Tiles';
$string['tilesamount'] = 'Amount of Tiles';
$string['tilesamountdesc'] = 'Selec the amount of tiles. Changing number of tiles requires you to save to view correct amount.';
$string['tilebtntext'] = 'Learn more';

for ($i=1; $i < 11 ; $i++) {
	$string['tile'.$i] = 'Tile '. $i  . ' background image';
	$string['tile'.$i.'desc'] = 'Add an image to your marketing spot, please make sure to make your pictures is the same size for better visual effect';

	$string['tile'.$i.'title'] = 'Tile '. $i  . ' Heading';
	$string['tile'.$i.'titledesc'] = 'Heading of the tile';

	$string['tile'.$i.'text'] = 'Tile '. $i  . ' text';
	$string['tile'.$i.'textdesc'] = 'Add a text which will be displayed in your tile';

	$string['tile'.$i.'link'] = 'Tile '. $i  . ' link';
	$string['tile'.$i.'linkdesc'] = 'Link to an internal or external page';
}

// -- Fonts

$string['fonts'] = 'Font Settings';
$string['fontname'] = 'Font';
$string['fontnamedesc'] = 'Default font used for text across the site';
$string['fontsubset'] = 'Font subset';
$string['fontsubsetdesc'] = 'Ability to use a subset if desired';
$string['fontheader'] = 'Header Font';
$string['fontheaderdesc'] = 'Default font for headings across the site';
$string['fontheadersubset'] = 'Header Font subset';
$string['fontheadersubsetdesc'] = 'Ability to use a subset if desired';

// -- Footer

$string['footer'] = 'Footer Settings';
$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Footnote to be displayed in the footer across the site';
$string['footerregionsamount'] = 'Number of block regions in the footer';
$string['footerregionsamountdesc'] = 'Ability to add numerous block regions to the footer';
for ($i=1; $i < 5 ; $i++) {
	$string['footerregion' . $i]='Footer region '. $i . ' width' ;
	$string['footerregion' . $i. 'desc']='Width of footer region '. $i . '.' ;
	$string['region-footerregion' . $alphas[$i-1].'']='Custom Footer Region '. $i  . '' ;
}
$string['footerbackground'] = 'Footer background colour';
$string['footerbackground_desc'] = 'Background colour of the footer.';
$string['footerbackgroundimage'] = 'Footer background image';
$string['footerbackgroundimagedesc'] = 'Footer background image replaces the background colour';
$string['footerlinkcolor'] = 'Footer text and link color';
$string['footerlinkcolordesc'] = 'Colour of the footer text and links';
$string['footericoncolor'] = 'Social media icon colour';
$string['footericoncolordesc'] = 'Colour of the social media icons';


// new
$string['linkhovercolor'] = 'Link hover colour';
$string['linkhovercolordesc'] = 'This sets the link hover colour for the theme.';
$string['buttontextcolor'] = 'Button text colour';
$string['buttontextcolordesc'] = 'This sets the colour for the text of buttons.';
$string['slidertitlecolor'] = 'Slider text background colour';
$string['slidertitlecolordesc'] = 'This sets the slider text background colour for the theme.';
$string['slidertextcolor'] = 'Slider text colour';
$string['slidertextcolordesc'] = 'This sets the slider text colour for the theme.';
$string['slidertextbackgroundcolor'] = 'Slider text background colour';
$string['slidertextbackgroundcolordesc'] = 'This sets the slider text background colour for the theme.';
$string['tileareatitle'] = 'Title of Marketing spots area';
$string['tileareatitledesc'] = 'Title of Marketing spots area';
$string['headingcolordesc'] = 'This sets the heading colour for the theme.';
$string['pluginname'] = 'ethink';
$string['marketingregion'] = 'Marketing Region';

$string['region-sidebar'] = 'Sidebar';
$string['region-side-admin'] = 'Admin';
for ($i=1; $i < 21 ; $i++) {
	$string['region-bregion' . $alphas[$i-1].'']='Custom Bottom Region '. $i  . '' ;
}
$string['region-landingregionone'] = 'Custom Landing Region 1';
$string['region-landingregiontwo'] = 'Custom Landing Region 2';
$string['region-landingregionthree'] = 'Custom Landing Region 3';
$string['region-landingregionfour'] = 'Custom Landing Region 4';
$string['region-landingregionfive'] = 'Custom Landing Region 5';
// for ($i=1; $i < 21 ; $i++) {
// 	$string['region-landingregion' . $alphas[$i-1].'']='Custom Landing Region '. $i  . '' ;
// }