<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   theme_ethink

For catalog: https://totaralearn.ethinksites.com/totara/catalog/index.php?catalog_fts=health&orderbykey=score&itemstyle=narrow
For course: https://totaralearn.ethinksites.com/course/search.php?search=health
For global: https://totaralearn.ethinksites.com/search/index.php?q=health


 */

defined('MOODLE_INTERNAL') || die();
$url= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
$parameter = 'toolbarsearchtext';


	
switch ($PAGE->theme->settings->searchsource) {
    case 0:
        $url= $CFG->wwwroot . '/totara/catalog/index.php';
        $parameter = 'catalog_fts';
        break;
    case 1:
        $url= $CFG->wwwroot . '/course/search.php';
        $parameter = 'search';
        break;
    case 2:
        $url= $CFG->wwwroot . '/search/index.php';
        $parameter = 'q';
        break;
    case 3:
        $url= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
        $parameter = 'toolbarsearchtext';
        break;
}

?>

<section class="CCsearch">
	<form autocomplete="off" action="<?php echo $url;?>" method="get" accept-charset="utf-8" id="toolbarsearch4" class="mform">
		<div style="display: none;"><input name="sesskey" type="hidden" value="<?php echo $USER->sesskey;?>">
			
		</div>
		
		<div id="fitem_id_toolbarsearchtext" class="fitem fitem_ftext  ">
			<div class="felement ftext" data-fieldtype="text" >
				<input name="<?php echo $parameter;?>" type="text" value="" id="id_toolbarsearchtext" placeholder="<?php // echo get_string('coursecatalogsearch', 'theme_ethink'); ?>">
			</div>
		</div>

		<div id="fitem_id_toolbarsearchbutton" class="fitem fitem_actionbuttons fitem_fsubmit ">
			<div class="felement fsubmit" data-fieldtype="submit">
				<input name="toolbarsearchbutton" value="Search" type="submit" id="id_toolbarsearchbutton">
			</div>
		</div>
	</form>
</section>
