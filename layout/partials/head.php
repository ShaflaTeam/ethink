<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   theme_ethink
 */

defined('MOODLE_INTERNAL') || die();

if (!empty($PAGE->theme->settings->favicon)) {
    $faviconurl = $PAGE->theme->setting_file_url('favicon', 'favicon');
} else {
    $faviconurl = $OUTPUT->favicon();
}
$fontname = str_replace(" ", "+", $PAGE->theme->settings->fontname);
$fontheadername = str_replace(" ", "+", $PAGE->theme->settings->fontheadername);

  
// Get the fonts subset.
if (!empty($PAGE->theme->settings->fontsubset)) {
    $fontssubset = '&subset=latin,'.$PAGE->theme->settings->fontsubset;
} else {
    $fontssubset = '';
}
if (!empty($PAGE->theme->settings->fontheadersubset)) {
    $fontheaderssubset = '&subset=latin,'.$PAGE->theme->settings->fontheadersubset;
} else {
    $fontheaderssubset = '';
}

$fontweight= ':100,300,400,600'


?>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $faviconurl; ?>" />
    <?php echo $OUTPUT->standard_head_html(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
    <!-- TODO FONT OPTIOS -->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700&display=swap" rel="stylesheet">
<?php // FONTOPTIONS BELOW
    if (!empty($fontname) && $fontname != 'default') {
?>
    <link href='https://fonts.googleapis.com/css?family=<?php echo $fontname.$fontweight.$fontssubset; ?>'
    rel='stylesheet'
    type='text/css'>
<?php
    }
?>
<?php // FONTOPTIONS BELOW
    if (!empty($fontheadername) && $fontheadername != 'default') {
?>
    <link href='https://fonts.googleapis.com/css?family=<?php echo $fontheadername.$fontweight.$fontheaderssubset; ?>'
    rel='stylesheet'
    type='text/css'>
<?php
    }
?>

    <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot.'/theme/'.$PAGE->theme->name.'/style/temp.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot.'/theme/'.$PAGE->theme->name.'/style/nav-side.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot.'/theme/'.$PAGE->theme->name.'/font/fa/css/font-awesome.min.css';?>">
    <link rel="stylesheet" type="text/css" href="<?php echo $CFG->wwwroot.'/theme/'.$PAGE->theme->name.'/style/tempmat.css';?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/'. $PAGE->theme->name .'/javascript/mh.js';?>"></script>
</head>
