

<div class="sidenav">
  <?php // require_once(dirname(__FILE__).'/search.php');  // TOTDO? ?>
	 <div class="mobile-menu-top-wrapper clearfix">
    <button class="nav-toggle">
        <div class="burgx"></div>
        <div class="burgx2"></div>
        <div class="burgx3"></div>
      </button>
    </div>

    <nav class="sidebar_totaraNav" role="navigation">
      <div class="hideable clearfix">
      	<div class="personal-info clearfix">

      		<?php
          if (!empty($PAGE->theme->settings->showavatar)) {
                global $USER;
                $fullname = fullname($USER, true);

                echo "<div class='user_pic clearfix'>";
                    if (isloggedin()) { echo $OUTPUT->user_picture($USER, array('size' => 150)); }
                echo "</div>";
                
                echo $OUTPUT->login_info(); 
          }
          ?>

      	</div>
      	

      <!-- class collapsed is removed -->
  		<?php 
      if ($PAGE->theme->settings->searchposition==9){
          echo $totara_core_renderer->navsearch();
      }

          echo $totara_core_renderer->sidebar($hasguestlangmenu, $nocustommenu);

          ?>

      </div>
      <?php echo $themerenderer->blocks_sidebar(); ?>

    </nav>
    <script type="text/javascript">
        var dropdown = $(".sidenav .totaraNav_prim--list_item_link");
        var i;

        for (i = 0; i < dropdown.length; i++) {
          dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
              dropdownContent.style.display = "none";
            } else {
              dropdownContent.style.display = "block";
            }
          });
        }

        // -- menu
        $(".nav-toggle").click(function(){
            $(".sidebar_totaraNav").toggleClass("user-navigation__visible");
            $(".nav-toggle").toggleClass("nav-toggle--true");
            $(".menu-background").toggleClass("menu-background--true");
        });

        $(".menu-background").click(function(){
            $("nav").toggleClass("user-navigation__visible");
            $(".nav-toggle").toggleClass("nav-toggle--true");
            $(".menu-background").toggleClass("menu-background--true");
            $(".dropdown-item").removeClass("dropdown-item--true");
            $(".search-item").removeClass("search-toggle--true");
        });

        $(".dropdown-toggle").click(function() {
            $(".search-item").removeClass("search-toggle--true");
            $(this).parent().toggleClass("dropdown-item--true");
            $('.dropdown-toggle').not(this).parent().removeClass('dropdown-item--true');
        });

    </script>


</div>
