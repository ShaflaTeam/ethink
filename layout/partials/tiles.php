<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   theme_ethink
 */

defined('MOODLE_INTERNAL') || die();

if (isset($PAGE->theme->settings->tilesamount)&&($PAGE->theme->settings->tilesamount > 0) ) {
?>

<section class="tiler">
    <div class="spots">
        <h2 class='tile-title marketingareatitle'>
        <?php echo  $PAGE->theme->settings->{'tileareatitle'}; ?>
        </h2>
        <ul class="tiles">
            <?php
             
            for ($i=1; $i <= $PAGE->theme->settings->tilesamount ; $i++) {
                echo "<li class='spot'>";
                echo '<img src="'.$PAGE->theme->setting_file_url('tile'.$i,'tile'.$i).'">';

                if (!empty($PAGE->theme->settings->{'tile'.$i.'title'})) {
                    if (($PAGE->theme->settings->{'tile'.$i.'title'}) !="") {

                        $tiletitle = ($PAGE->theme->settings->{'tile'.$i.'title'});
                        $tiletext = ($PAGE->theme->settings->{'tile'.$i.'text'});
                        $tilelink = ($PAGE->theme->settings->{'tile'.$i.'link'});

                        echo "<div id='tile-info' class='media-bg'>";
                            echo "<div id='tile-info-wrapper'>";
                                echo "<h2 class='tile-title'>".$tiletitle."</h2>";
                                echo "<p class='tile-text'>".$tiletext."</p>"; 
                            echo "</div>";

                            echo "<a href='".$tilelink."' class='tile-has-link btn'>";
                                echo get_string('tilebtntext', 'theme_ethink');
                            echo "</a>";
                        echo "</div>";
                }}                            
                
                echo "</li>";
            }
        
            // else {
            //     echo "<div class='shfnoimg'>";
            //     echo '<img src="'. $CFG->wwwroot.'/theme/'.$PAGE->theme->name.'/pix/sampletop.jpg">';
            //         echo "<div id='tile-info' class='media-bg'>";
            //             echo "<a class='tile-has-link'>";
            //                 echo "<div id='tile-info-wrapper'>";
            //                     echo "<p class='tile-title'></p>";
            //                     echo "<p class='tile-text'></p>"; 
            //                 echo "</div>";
            //             echo "</a>";
            //         echo "</div>";
            //     echo "</div>";
                
            // }
            ?>
        </ul>

    </div>
</section>
<?php } ?>