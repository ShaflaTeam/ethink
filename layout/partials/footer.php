<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @package   theme_roots
 */

defined('MOODLE_INTERNAL') || die();

?>

<!-- Scroll to top button  -->
<div class="scrollBtn">
    <?php
        if (!empty($PAGE->theme->settings->topbutton)) {
            $topbutton = $PAGE->theme->setting_file_url('topbutton', 'topbutton');
            echo '<img src="' . $topbutton . '">';
        }
    ?>
</div>
<script type="text/javascript">

        // -- scroll top btn
        $(window).scroll(function(){

            // -- scroll top btn
            if ($(this).scrollTop() > 100) {
                $('.scrollBtn').fadeIn();
            } else {
                $('.scrollBtn').fadeOut();
            }

            if ($(this).scrollTop() > 10) {
                $('#has-logo').addClass('has-logo-scroll');
            } else {
                $('#has-logo').removeClass('has-logo-scroll');
            }

        });
        
        $('.scrollBtn').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
        
        $(window).load(function(){

            // -- site loader
            $(".site-loader").fadeOut("slow");

     

            
        });

</script>

<footer id="page-footer" class="page-footer">

    <?php 
    echo $themerenderer->blocks_footer();
    ?>

    <div class="container-fluid">
        <?php
        if (!empty($PAGE->theme->settings->footnote)) {
            echo '<div class="footnote">'.format_text($PAGE->theme->settings->footnote).'</div>';
        }
        ?>
        <div class="footer-container footer-container-images">
            <div class="footer-container-images-content">
                <?php
                    echo $themerenderer->socialicons();
                ?>
            </div>
        </div>
    </div>
    <?php echo $OUTPUT->login_info(); // TODO LOGININFO FIX MOVE?> 


</footer>

<!-- FILES RELATED CSS -->
<?php require($CFG->dirroot.'/theme/ethink/cssfiles.php'); ?>

<script type="text/javascript">
    $( document ).ready(function() {
        // $('.logininfo .btn-primary').prependTo(".totaraNav_prim--side .menubar > li");
        $('#side-menu .custom_totaraNav_prim--list_item_hasChildren .totaraNav_prim--list_item_label').after('<div class="arror-up"></div>');
        $(".navsearch-button .fa-search").click(function() {
            $(".navsearch input#id_toolbarsearchtext").focus();
            $(".navsearch-button").css("z-index", "3");
        });
        // if ($('.ethinknav .etflexcol').empty()){
        //     $(this).css('padding','0');
        // }

        // if ($('.ethinknav .etflexcol').is(':blank')){
            // $(this).css('padding','0');
        //     console.log("xxx");
        // }
        function isEmpty( el ){
            return !$.trim(el.html())
        }
        if ($(window).width() < 992) {
        (function($) {
            $(function() {
                $('.ethinknav .etflexcol').each(function(){
                    if (isEmpty($(this))) {
                        $(this).css('padding','0');
                    }
                });
            });
        })(jQuery);
    }
    });
</script>
