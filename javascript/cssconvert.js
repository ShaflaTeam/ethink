
function ethink_course_cssconvert(arg) {
    $.each(arg, function (id, obj) {
        var icon_old = $('li.activity.'+obj.modname+'[id="module-'+id+'"]').find('span.activityicon');

        var span = $('<span />').attr('class', 'ethinkactivityicon')
                .css('background-image', 'url(\''+obj.state_image+'\')');
        icon_old.parent().prepend(span);        
        icon_old.hide();                
    });

return true;

};
