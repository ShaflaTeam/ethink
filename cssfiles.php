
<style>

html,
body {

    font-family: "<?php echo $PAGE->theme->settings->fontname; ?>", sans-serif;
}

/* Headings */
button,
input.submit,
input[type="submit"],
input[type="reset"],
input[type="button"],
#id_buttonfield,
a.link-as-button,
a.btn ,
.spot .tile-title,
.totaraNav,
h1,h2,h3,h4,h5,h6{
     font-family: "<?php echo $PAGE->theme->settings->fontheadername; ?>", sans-serif;

}


/*FOOTER*/

<?php if (!empty($PAGE->theme->settings->footerbackgroundimage)){ ?>
#page-footer{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('footerbackgroundimage', 'footerbackgroundimage');?>");
}<?php }?>

/*FRONTPAGE*/

<?php if (!empty($PAGE->theme->settings->footerbackgroundimage)){ ?>
#block-region-regiona{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('footerbackgroundimage', 'footerbackgroundimage');?>");
}<?php }?>

<?php if (!empty($PAGE->theme->settings->regionbg1)){ ?>
#block-region-regiona{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg1', 'regionbg1');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg2)){ ?>
#block-region-regionb{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg2', 'regionbg2');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg3)){ ?>
#block-region-regionc{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg3', 'regionbg3');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg4)){ ?>
#block-region-regiond{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg4', 'regionbg4');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg5)){ ?>
#block-region-regione{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg5', 'regionbg5');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg6)){ ?>
#block-region-regionf{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg6', 'regionbg6');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg7)){ ?>
#block-region-regiong{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg7', 'regionbg7');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg8)){ ?>
#block-region-regionh{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg8', 'regionbg8');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg9)){ ?>
#block-region-regioni{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg9', 'regionbg9');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regionbg10)){ ?>
#block-region-regionj{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('regionbg10', 'regionbg10');?>");
}<?php }?>

<?php if (!empty($PAGE->theme->settings->regioncolor1)){ ?>
#block-region-regiona{
	background-color: <?php echo $PAGE->theme->settings->regioncolor1;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor2)){ ?>
#block-region-regionb{
	background-color: <?php echo $PAGE->theme->settings->regioncolor2;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor3)){ ?>
#block-region-regionc{
	background-color: <?php echo $PAGE->theme->settings->regioncolor3;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor4)){ ?>
#block-region-regiond{
	background-color: <?php echo $PAGE->theme->settings->regioncolor4;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor5)){ ?>
#block-region-regione{
	background-color: <?php echo $PAGE->theme->settings->regioncolor5;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor6)){ ?>
#block-region-regionf{
	background-color: <?php echo $PAGE->theme->settings->regioncolor6;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor7)){ ?>
#block-region-regiong{
	background-color: <?php echo $PAGE->theme->settings->regioncolor7;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor8)){ ?>
#block-region-regionh{
	background-color: <?php echo $PAGE->theme->settings->regioncolor8;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor9)){ ?>
#block-region-regioni{
	background-color: <?php echo $PAGE->theme->settings->regioncolor9;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->regioncolor10)){ ?>
#block-region-regionj{
	background-color: <?php echo $PAGE->theme->settings->regioncolor10;?>;
}<?php }?>




/*BOTTOM FP REGIONS*/
<?php if (!empty($PAGE->theme->settings->bregionbg1)){ ?>
#block-region-bregiona{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('bregionbg1', 'bregionbg1');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->bregionbg2)){ ?>
#block-region-bregionb{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('bregionbg2', 'bregionbg2');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->bregionbg3)){ ?>
#block-region-bregionc{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('bregionbg3', 'bregionbg3');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->bregionbg4)){ ?>
#block-region-bregiond{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('bregionbg4', 'bregionbg4');?>");
}<?php }?>


<?php if (!empty($PAGE->theme->settings->bregioncolor1)){ ?>
#block-region-bregiona{
	background-color: <?php echo $PAGE->theme->settings->bregioncolor1;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->bregioncolor2)){ ?>
#block-region-bregionb{
	background-color: <?php echo $PAGE->theme->settings->bregioncolor2;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->bregioncolor3)){ ?>
#block-region-bregionc{
	background-color: <?php echo $PAGE->theme->settings->bregioncolor3;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->bregioncolor4)){ ?>
#block-region-bregiond{
	background-color: <?php echo $PAGE->theme->settings->bregioncolor4;?>;
}<?php }?>













/*DASHBOARD */


<?php if (!empty($PAGE->theme->settings->dregionbg1)){ ?>
#block-region-dregiona{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg1', 'dregionbg1');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg2)){ ?>
#block-region-dregionb{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg2', 'dregionbg2');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg3)){ ?>
#block-region-dregionc{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg3', 'dregionbg3');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg4)){ ?>
#block-region-dregiond{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg4', 'dregionbg4');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg5)){ ?>
#block-region-dregione{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg5', 'dregionbg5');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg6)){ ?>
#block-region-dregionf{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg6', 'dregionbg6');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg7)){ ?>
#block-region-dregiong{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg7', 'dregionbg7');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg8)){ ?>
#block-region-dregionh{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg8', 'dregionbg8');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg9)){ ?>
#block-region-dregioni{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg9', 'dregionbg9');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregionbg10)){ ?>
#block-region-dregionj{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dregionbg10', 'dregionbg10');?>");
}<?php }?>

<?php if (!empty($PAGE->theme->settings->dregioncolor1)){ ?>
#block-region-dregiona{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor1;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor2)){ ?>
#block-region-dregionb{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor2;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor3)){ ?>
#block-region-dregionc{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor3;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor4)){ ?>
#block-region-dregiond{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor4;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor5)){ ?>
#block-region-dregione{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor5;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor6)){ ?>
#block-region-dregionf{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor6;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor7)){ ?>
#block-region-dregiong{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor7;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor8)){ ?>
#block-region-dregionh{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor8;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor9)){ ?>
#block-region-dregioni{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor9;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dregioncolor10)){ ?>
#block-region-dregionj{
	background-color: <?php echo $PAGE->theme->settings->dregioncolor10;?>;
}<?php }?>




/*BOTTOM DASH REGIONS*/
<?php if (!empty($PAGE->theme->settings->dbregionbg1)){ ?>
#block-region-dbregiona{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dbregionbg1', 'dbregionbg1');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dbregionbg2)){ ?>
#block-region-dbregionb{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dbregionbg2', 'dbregionbg2');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dbregionbg3)){ ?>
#block-region-dbregionc{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dbregionbg3', 'dbregionbg3');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dbregionbg4)){ ?>
#block-region-dbregiond{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('dbregionbg4', 'dbregionbg4');?>");
}<?php }?>


<?php if (!empty($PAGE->theme->settings->dbregioncolor1)){ ?>
#block-region-dbregiona{
	background-color: <?php echo $PAGE->theme->settings->dbregioncolor1;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dbregioncolor2)){ ?>
#block-region-dbregionb{
	background-color: <?php echo $PAGE->theme->settings->dbregioncolor2;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dbregioncolor3)){ ?>
#block-region-dbregionc{
	background-color: <?php echo $PAGE->theme->settings->dbregioncolor3;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->dbregioncolor4)){ ?>
#block-region-dbregiond{
	background-color: <?php echo $PAGE->theme->settings->dbregioncolor4;?>;
}<?php }?>







/* GENERAL */


<?php if (!empty($PAGE->theme->settings->gregionbg1)){ ?>
#block-region-gregiona{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg1', 'gregionbg1');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg2)){ ?>
#block-region-gregionb{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg2', 'gregionbg2');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg3)){ ?>
#block-region-gregionc{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg3', 'gregionbg3');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg4)){ ?>
#block-region-gregiond{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg4', 'gregionbg4');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg5)){ ?>
#block-region-gregione{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg5', 'gregionbg5');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg6)){ ?>
#block-region-gregionf{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg6', 'gregionbg6');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg7)){ ?>
#block-region-gregiong{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg7', 'gregionbg7');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg8)){ ?>
#block-region-gregionh{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg8', 'gregionbg8');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg9)){ ?>
#block-region-gregioni{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg9', 'gregionbg9');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregionbg10)){ ?>
#block-region-gregionj{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gregionbg10', 'gregionbg10');?>");
}<?php }?>

<?php if (!empty($PAGE->theme->settings->gregioncolor1)){ ?>
#block-region-gregiona{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor1;?>;
}<?php }?>

<?php if (!empty($PAGE->theme->settings->gregioncolor2)){ ?>
#block-region-gregionb{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor2;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor3)){ ?>
#block-region-gregionc{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor3;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor4)){ ?>
#block-region-gregiond{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor4;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor5)){ ?>
#block-region-gregione{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor5;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor6)){ ?>
#block-region-gregionf{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor6;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor7)){ ?>
#block-region-gregiong{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor7;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor8)){ ?>
#block-region-gregionh{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor8;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor9)){ ?>
#block-region-gregioni{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor9;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gregioncolor10)){ ?>
#block-region-gregionj{
	background-color: <?php echo $PAGE->theme->settings->gregioncolor10;?>;
}<?php }?>




/*BOTTOM GENERAL REGIONS*/
<?php if (!empty($PAGE->theme->settings->gbregionbg1)){ ?>
#block-region-gbregiona{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gbregionbg1', 'gbregionbg1');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gbregionbg2)){ ?>
#block-region-gbregionb{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gbregionbg2', 'gbregionbg2');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gbregionbg3)){ ?>
#block-region-gbregionc{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gbregionbg3', 'gbregionbg3');?>");
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gbregionbg4)){ ?>
#block-region-gbregiond{
	background-image: url("<?php echo $PAGE->theme->setting_file_url('gbregionbg4', 'gbregionbg4');?>");
}<?php }?>


<?php if (!empty($PAGE->theme->settings->gbregioncolor1)){ ?>
#block-region-gbregiona{
	background-color: <?php echo $PAGE->theme->settings->gbregioncolor1;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gbregioncolor2)){ ?>
#block-region-gbregionb{
	background-color: <?php echo $PAGE->theme->settings->gbregioncolor2;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gbregioncolor3)){ ?>
#block-region-gbregionc{
	background-color: <?php echo $PAGE->theme->settings->gbregioncolor3;?>;
}<?php }?>
<?php if (!empty($PAGE->theme->settings->gbregioncolor4)){ ?>
#block-region-gbregiond{
	background-color: <?php echo $PAGE->theme->settings->gbregioncolor4;?>;
}<?php }?>

<?php if ($PAGE->theme->settings->showsidebar==0){ ?>
#side-menu {
	display: none;
}<?php }?>

<?php if ($PAGE->theme->settings->showsidebar==0){ ?>
	@media (max-width: 991px) {
		#side-menu {
			display: block;
		}	
	}
<?php }?>

</style>