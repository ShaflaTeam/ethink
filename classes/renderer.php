<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author    Joby Harding <joby.harding@totaralearning.com>
 * @package   theme_roots
 */

defined('MOODLE_INTERNAL' || die());

class theme_ethink_renderer extends theme_roots_renderer {

    /**
     * Implements standard 'convenience method' renderer pattern.
     *
     * @deprecated since 12.0
     */
    public function page_footer_nav($menudata) {
        $renderable = new theme_ethink\output\page_footer_nav($menudata);
        return $this->render($renderable);
    }

    /**
     * Render method for page_footer_nav renderables.
     *
     * @deprecated since 12.0
     */
    protected function render_page_footer_nav($renderable) {
        $templatecontext = $renderable->export_for_template($this);
        return $this->render_from_template('theme_ethink/page_footer_nav', $templatecontext);
    }

    public function blocks_flexgrid(){
        global $PAGE;
        $alphas = range('a', 'z');
        if (empty($PAGE->theme->settings->regionsamount) || ($PAGE->theme->settings->regionsamount == 0 )) {
            return '';
        }
        $html='<div id="flexgrid">';
        $tag = 'div';
        for ($i=1; $i <= $PAGE->theme->settings->regionsamount ; $i++) {
        
            $wclass = 'ethinkflex'.($PAGE->theme->settings->{'region'.$alphas[$i-1]});
            $html .= $this->output->blocks( 'fpregion'.$alphas[$i-1], $wclass, $tag);
        }
        $html.='</div>';
        
        return $html;
    }
    public function blocks_bflexgrid(){
        global $PAGE;
        $alphas = range('a', 'z');
        if (empty($PAGE->theme->settings->bregionsamount) || ($PAGE->theme->settings->bregionsamount == 0 )) {
            return '';
        }
        $html='<div id="flexgrid">';
        $tag = 'div';
        for ($i=1; $i <= $PAGE->theme->settings->bregionsamount ; $i++) {
        
            $wclass = 'ethinkflex'.($PAGE->theme->settings->{'bregion'.$alphas[$i-1]});
            $html .= $this->output->blocks( 'bregion'.$alphas[$i-1], $wclass, $tag);
        }
        $html.='</div>';
        
        return $html;
    }
    public function blocks_dflexgrid(){
        global $PAGE;
        $alphas = range('a', 'z');
        if (empty($PAGE->theme->settings->dregionsamount) || ($PAGE->theme->settings->dregionsamount == 0 )) {
            return '';
        }
        $html='<div id="flexgrid">';
        $tag = 'div';
        for ($i=1; $i <= $PAGE->theme->settings->dregionsamount ; $i++) {
        
            $wclass = 'ethinkflex'.($PAGE->theme->settings->{'dregion'.$alphas[$i-1]});
            $html .= $this->output->blocks( 'dregion'.$alphas[$i-1], $wclass, $tag);
        }
        $html.='</div>';
        
        return $html;
    }
    public function blocks_dbflexgrid(){
        global $PAGE;
        $alphas = range('a', 'z');
        if (empty($PAGE->theme->settings->dbregionsamount) || ($PAGE->theme->settings->dbregionsamount == 0 )) {
            return '';
        }
        $html='<div id="bflexgrid">';
        $tag = 'div';
        for ($i=1; $i <= $PAGE->theme->settings->dbregionsamount ; $i++) {
        
            $wclass = 'ethinkflex ethinkflex'.($PAGE->theme->settings->{'dbregion'.$alphas[$i-1]});
            $html .= $this->output->blocks( 'dbregion'.$alphas[$i-1], $wclass, $tag);
        }
        $html.='</div>';
        
        return $html;
    }
    public function blocks_gflexgrid(){
        global $PAGE;
        $alphas = range('a', 'z');
        if (empty($PAGE->theme->settings->gregionsamount) || ($PAGE->theme->settings->gregionsamount == 0 )) {
            return '';
        }
        $html='<div id="flexgrid">';
        $tag = 'div';
        for ($i=1; $i <= $PAGE->theme->settings->gregionsamount ; $i++) {
        
            $wclass = 'ethinkflex'.($PAGE->theme->settings->{'gregion'.$alphas[$i-1]});
            $html .= $this->output->blocks( 'gregion'.$alphas[$i-1], $wclass, $tag);
        }
        $html.='</div>';
        
        return $html;
    }
    public function blocks_gbflexgrid(){
        global $PAGE;
        $alphas = range('a', 'z');
        if (empty($PAGE->theme->settings->gbregionsamount) || ($PAGE->theme->settings->gbregionsamount == 0 )) {
            return '';
        }
        $html='<div id="bflexgrid">';
        $tag = 'div';
        for ($i=1; $i <= $PAGE->theme->settings->gbregionsamount ; $i++) {
        
            $wclass = 'ethinkflex ethinkflex'.($PAGE->theme->settings->{'gbregion'.$alphas[$i-1]});
            $html .= $this->output->blocks( 'gbregion'.$alphas[$i-1], $wclass, $tag);
        }
        $html.='</div>';
        
        return $html;
    }


    public function blocks_footer(){
        global $PAGE;
        $alphas = range('a', 'z');
        if (empty($PAGE->theme->settings->footerregionsamount) || ($PAGE->theme->settings->footerregionsamount == 0 )) {
            return '';
        }
        $html='<div id="footergrid">';
        $tag = 'div';
        for ($i=1; $i <= $PAGE->theme->settings->footerregionsamount ; $i++) {
        
            $wclass = 'ethinkflex'.($PAGE->theme->settings->{'footerregion'.$alphas[$i-1]});
            $html .= $this->output->blocks( 'footerregion'.$alphas[$i-1], $wclass, $tag);
        }
        $html.='</div>';
    
        return $html;
    }
    public function blocks_landingregionone(){

        $tag = 'div';
        $class = 'landingregionone';
        $html = $this->output->blocks( 'landingregionone', $class, $tag);

        return $html;
    }
    public function blocks_landingregiontwo(){

        $tag = 'div';
        $class = 'landingregiontwo';
        $html = $this->output->blocks( 'landingregiontwo', $class, $tag);

        return $html;
    }
    public function blocks_sidebar(){

        $tag = 'div';
        $class = 'sidebar';
        $html = $this->output->blocks( 'sidebar', $class, $tag);

        return $html;
    }

    public function socialicons() {
        global $PAGE;

        // Social Settings
        $hasfacebook    = (empty($PAGE->theme->settings->facebook)) ? false : $PAGE->theme->settings->facebook;
        $hastwitter     = (empty($PAGE->theme->settings->twitter)) ? false : $PAGE->theme->settings->twitter;
        $hasgoogleplus  = (empty($PAGE->theme->settings->googleplus)) ? false : $PAGE->theme->settings->googleplus;
        $haslinkedin    = (empty($PAGE->theme->settings->linkedin)) ? false : $PAGE->theme->settings->linkedin;
        $hasyoutube     = (empty($PAGE->theme->settings->youtube)) ? false : $PAGE->theme->settings->youtube;
        $hasflickr      = (empty($PAGE->theme->settings->flickr)) ? false : $PAGE->theme->settings->flickr;
        $haspinterest   = (empty($PAGE->theme->settings->pinterest)) ? false : $PAGE->theme->settings->pinterest;
        $hasinstagram   = (empty($PAGE->theme->settings->instagram)) ? false : $PAGE->theme->settings->instagram;
        $hasskype       = (empty($PAGE->theme->settings->skype)) ? false : $PAGE->theme->settings->skype;
        $hassocialnetworks = ($hasfacebook || $hastwitter || $hasgoogleplus || $hasflickr || $hasinstagram || $haslinkedin || $haspinterest || $hasskype || $haslinkedin || $hasyoutube);

        $html = '';
        if ($hassocialnetworks) {
            $html = html_writer::start_tag('div', array('class'=>'socialnetworkicons'));
            $html .= html_writer::start_tag('ul', array('class'=>'socialsnetworkul'));
                if ($hasgoogleplus) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-google-plus'));
                    $anchor = html_writer::link($hasgoogleplus, $iconclass, array('class'=>'googleplus', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($hastwitter) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-twitter'));
                    $anchor = html_writer::link($hastwitter, $iconclass, array('class'=>'twitter', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($hasfacebook) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-facebook'));
                    $anchor = html_writer::link($hasfacebook, $iconclass,  array('class'=>'facebook', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($haslinkedin) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-linkedin'));
                    $anchor = html_writer::link($haslinkedin, $iconclass,  array('class'=>'linkedin', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($hasyoutube) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-youtube'));
                    $anchor = html_writer::link($hasyoutube, $iconclass,  array('class'=>'youtube', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($hasflickr) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-flickr'));
                    $anchor = html_writer::link($hasflickr, $iconclass,  array('class'=>'flickr', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($haspinterest) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-pinterest'));
                    $anchor = html_writer::link($haspinterest, $iconclass,  array('class'=>'pinterest', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($hasinstagram) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-instagram'));
                    $anchor = html_writer::link($hasinstagram, $iconclass,  array('class'=>'instagram', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
                if ($hasskype) {
                    $iconclass = html_writer::tag('i', '', array('class'=>'fa fa-fw fa-skype icon-light'));
                    $anchor = html_writer::link($hasskype, $iconclass,  array('class'=>'skype', 'target'=>'_blank'));
                    $html .= html_writer::tag('li', $anchor);
                }
            $html .= html_writer::end_tag('ul');
            $html .= html_writer::end_tag('div');
        }
        return $html;
    }
    public function ethink_get_bodyclasses_from_cf(){
        global $USER,$CFG;
        require_once($CFG->dirroot.'/user/profile/lib.php');
        $data = profile_user_record($USER->id);
        if (!empty($data->ethinkclasses)) {
         return $data->ethinkclasses;
        }
        
    }
}
