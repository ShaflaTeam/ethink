<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author    Joby Harding <joby.harding@totaralearning.com>
 * @package   theme_ethink
 */

namespace theme_ethink;

defined('MOODLE_INTERNAL') || die();

class css_processor {

    const TOKEN_ENABLEOVERRIDES_START = '[[enablestyleoverrides:start]]';
    const TOKEN_ENABLEOVERRIDES_END = '[[enablestyleoverrides:end]]';

    // We use static properties rather than class constants
    // so that the class may be reused / overridden in other
    // themes. Otherwise all inheriting classes would be stuck
    // with the ethink default colours.

    /**
     * @var string
     */
    public static $DEFAULT_LINKCOLOR = '#169bac';

    /**
     * @var string
     */
    public static $DEFAULT_LINKVISITEDCOLOR = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_HEADINGCOLOR = '#000';
    /**
     * @var string
     */
    public static $DEFAULT_BUTTONCOLOR = '#438897';

    /**
     * @var string
     */
    public static $DEFAULT_CONTENTBACKGROUND = '#FFFFFF';

    /**
     * @var string
     */
    public static $DEFAULT_BODYBACKGROUND = '#FFFFFF';

    /**
     * @var string
     */
    public static $DEFAULT_TEXTCOLOR = '#000000';

    /**
     * @var string
     */
    public static $DEFAULT_NAVTEXTCOLOR = '#36A5A6';

    /**
     * @var string
     */
    public static $DEFAULT_LOGINBOXBACKGROUND = '#169bac';

    /**
     * @var string
     */
    public static $DEFAULT_FOOTERBACKGROUND = '#0f2044';

    /**
     * @var string
     */
    
    public static $DEFAULT_FOOTERLINKCOLOR = '#ffffff';

    /**
     * @var string
     */
    public static $DEFAULT_LOGINPAGEBACKGROUNDCOLOR = '#FFFFFF';

    /**
     * @var string
     */
    public static $DEFAULT_FOOTERTEXTCOLOR = '#57595b';

    /**
     * @var string
     */
    public static $DEFAULT_FOOTERICONCOLOR = '#FFFFFF';

    /**
     * @var string
     */
    public static $DEFAULT_SLIDERTITLECOLOR = '#000000';

    /**
     * @var string
     */
    public static $DEFAULT_SLIDERTEXTCOLOR = '#000000';

    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARBGC = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARBURGERBACKGROUNDC = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARBURGERBACKGROUNDCH = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARBURGERC = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARBURGERCH = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARBURGERCO = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARTEXTC = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARITEMCH = '#0f2044';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARITEMCA = '#0f2044';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARITEMCS = '#ffffff';
    /**
     * @var string
     */





    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARITEMTEXTCH = '#ffffff';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARITEMTEXTCA = '#ffffff';
    /**
     * @var string
     */
    public static $DEFAULT_SIDEBARITEMTEXTCS = '#0f2044';
    /**
     * @var string
     */








    public static $DEFAULT_HEADERBACKGROUNDCOLORTOP = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERBACKGROUNDCOLORMIDDLE = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERBACKGROUNDCOLORBOTTOM = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERBACKGROUNDCOLOREXPANDLIST = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERCOLOR = '#000000';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERDROPDOWNCOLOR = '#000000';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERDROPDOWNCOLORH = '#000000';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERITEMBACKGROUNDCOLORA = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERITEMCOLORA = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERITEMCOLORH = '#ffffff';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERITEMBACKGROUNDCOLORH = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERADMINPANELBACKGROUNDCOLOR = '#FFFFFF';
    /**
     * @var string
     */
    public static $DEFAULT_HEADERADMINPANELTEXTCOLOR = '#000000';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERADMINPANELLINKCOLOR = '#000000';
    /**
     * @var string
     */
    public static $DEFAULT_LINKHOVERCOLOR = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_BUTTONTEXTCOLORH = '#ffffff';
    /**
     * @var string
     */
    public static $DEFAULT_BUTTONTEXTCOLOR = '#ffffff';
    /**
     * @var string
     */
    public static $DEFAULT_BUTTONBACKGROUNDCOLORH = '#169bac';
    /**
     * @var string
     */
    public static $DEFAULT_DBUTTONCOLOR = '#438897';
    /**
     * @var string
     */
    public static $DEFAULT_DBUTTONTEXTCOLORH = '#ffffff';
    /**
     * @var string
     */
    public static $DEFAULT_DBUTTONTEXTCOLOR = '#ffffff';
    /**
     * @var string
     */
    public static $DEFAULT_DBUTTONBACKGROUNDCOLORH = '#169bac';

    /**
     * @var string
     */
    public static $DEFAULT_SLIDERTEXTBACKGROUNDCOLOR = '#ffffff';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERNEWSBACKGROUNDCOLOR = '#ffffff';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERNEWSTEXTCOLOR = '#000000';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERNEWSLINKCOLOR = '#000000';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERNEWSUNREADBGC = '#ffffff';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERNEWSUNREADBGCH = '#ffffff';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERNEWSCOUNTBGC = '#000000';

    /**
     * @var string
     */
    public static $DEFAULT_HEADERNEWSCOUNTC = '#ffffff';

    /**

     * @var string
     */
    public static $DEFAULT_CUSTOMCSS = '';

    /**
     * @var \theme_config
     */
    protected $theme = null;

    /**
     * Constructor.
     *
     * @param string $css Styles to be processed.
     * @param theme_config|null $theme Theme object.
     */
    public function __construct($theme = null) {
        global $THEME;

        $this->theme = ($theme === null) ? $THEME : $theme;
    }

    /**
     * Return settings CSS delimited by start and end tokens.
     *
     * An array is always returned containing matches.
     *
     * @param string $css
     * @return array
     */
    public function get_settings_css($css) {
        $matches = array();
        $start = preg_quote(self::TOKEN_ENABLEOVERRIDES_START);
        $end = preg_quote(self::TOKEN_ENABLEOVERRIDES_END);
        if (preg_match_all("/{$start}.*?{$end}/s", $css, $matches) > 0) {
            return $matches[0];
        }
        return array();
    }

    /**
     * Replace colour placeholders including variants based on given substitutions.
     *
     * @param array $substitutions Key => Value
     * @param string $css
     * @return string
     */
    public function replace_colours($substitutions, $css) {
        return totara_theme_generate_autocolors($css, $this->theme, $substitutions);
    }

    /**
     * Replace the customcss token with given styles.
     *
     * @param array $defaults In the form token => replacement.
     * @param string $css
     * @return string
     */
    public function replace_tokens(array $defaults, $css) {
        $theme = $this->theme;
        $replacements = array();
        foreach ($defaults as $settingname => $default) {
            $replacement = isset($theme->settings->{$settingname}) ? $theme->settings->{$settingname} : $default;
            $replacements["[[setting:{$settingname}]]"] = $replacement;
        }
        return str_replace(array_keys($replacements), array_values($replacements), $css);
    }

    /**
     * Remove the settings CSS delimiter tokens.
     *
     * @param string $css
     * @return string
     */
    public function remove_delimiters($css) {
        $replacements = array(
            self::TOKEN_ENABLEOVERRIDES_START => '',
            self::TOKEN_ENABLEOVERRIDES_END => ''
        );
        return str_replace(array_keys($replacements), array_values($replacements), $css);
    }

}
