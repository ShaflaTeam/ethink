<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2016 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @copyright 2016 onwards Totara Learning Solutions LTD
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author    Joby Harding <joby.harding@totaralearning.com>
 * @package   theme_cwbrunel
 */

defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once("{$CFG->dirroot}/totara/core/renderer.php");

class theme_ethink_totara_core_renderer extends totara_core_renderer {

    /**
     * Return the currently selected page's menu item or null.
     *
     * Note: This function may mutate the contextdata to ensure the
     * .selected class is consistently applied.
     *
     * @param $contextdata stdClass
     * @return array
     */
    protected function current_selected_item(&$contextdata) {

        $currentitem = null;
        $itemindex = -1;
        $childselected = false;

        foreach (array_values($contextdata->menuitems) as $i => $navitem) {

            if ($currentitem !== null) {
                break;
            }

            if ($navitem['class_isselected']) {
                if (empty($navitem['children'])) {
                    $currentitem = $navitem;
                    $itemindex = $i;
                }
            }

            foreach ($navitem['children'] as $childitem) {
                if ($childitem['class_isselected']) {
                    $currentitem = $navitem;
                    $itemindex = $i;
                    $childselected = true;
                }
            }
        }

        if ($currentitem !== null) {
            // the .selected class is not consistently added so normalize TL-10596
            $contextdata->menuitems[$itemindex]['class_isselected'] = true;
            // Add a class so we know it is the child item which is active.
            if ($childselected) {
                $contextdata->menuitems[$itemindex]['class_child_isselected'] = true;
            }
        }

        return $currentitem;
    }

    /**
     * Render menu.
     *
     * @param \totara_core\output\totara_menu $totaramenu
     * @return string
     */
    protected function render_totara_menu(totara_core\output\totara_menu $totaramenu) {

        global $OUTPUT;

        $contextdata = $totaramenu->export_for_template($this);
        $currentselected = $this->current_selected_item($contextdata);
        $contextdata->subnav = array();

        if ($currentselected !== null) {
            if (!empty($currentselected['children'])) {
                $contextdata->subnav = $currentselected['children'];
            }
        }

        $contextdata->subnav_has_items = !empty($contextdata->subnav);

        // If the first item is the default home link replace it with an icon.
        if (!empty($contextdata->menuitems)) {
            $firstitem = $contextdata->menuitems[0];
            if ($firstitem['class_name'] === 'home') {
                $icon = $OUTPUT->flex_icon('home', array('alt' => $firstitem['linktext']));
                $contextdata->menuitems[0]['linktext'] = $icon;
            }
        }

        return $this->render_from_template('totara_core/totara_menu', $contextdata);
    }

    /**
     * Render the masthead.
     *
     * @return string the html output
     */
    public function masthead(bool $hasguestlangmenu = true, bool $nocustommenu = false) {
        global $CFG,$USER,$PAGE;


        //Check if menu is inline with elements 
        $menuinline = 0;
        if ($nocustommenu || !empty($this->page->layout_options['nototaramenu']) || !empty($this->page->layout_options['nocustommenu'])) {
            // No totara menu, or the old legacy no custom menu, in which case DO NOT generate the totara menu, its costly.
            $mastheadmenudata = new stdClass;
        } else {
            $menudata = totara_build_menu();
            $mastheadmenu = new totara_core\output\masthead_menu($menudata);
            $mastheadmenudata = $mastheadmenu->export_for_template($this->output);
        }

        $mastheadlogo = new theme_ethink\output\masthead_logo();

        $mastheaddata = new stdClass();
        $mastheaddata->masthead_lang = $hasguestlangmenu && (!isloggedin() || isguestuser()) ? $this->output->lang_menu() : '';

        $mastheaddata->emptytopleft = true;
        $mastheaddata->emptytopmiddle = true;
        $mastheaddata->emptytopright = true;
        $mastheaddata->emptymiddleleft = true;
        $mastheaddata->emptymiddlemiddle = true;
        $mastheaddata->emptymiddleright = true;
        $mastheaddata->emptybottomleft = true;
        $mastheaddata->emptybottommiddle = true;
        $mastheaddata->emptybottomright = true;

        // LOGOPOSITION
        $mastheaddata->masthead_logo = '';
        if (!empty($PAGE->theme->settings->logoposition)) {
            if (($PAGE->theme->settings->logoposition == 0)) {
                $mastheaddata->masthead_logo = '';
            }

            // TOPS
            if (($PAGE->theme->settings->logoposition == 1)) {
                $mastheaddata->masthead_logotl = $mastheadlogo->export_for_template($this->output);
                $mastheaddata->emptytopleft = flase;
            }
            if (($PAGE->theme->settings->logoposition == 2)) {
                $mastheaddata->masthead_logotc = $mastheadlogo->export_for_template($this->output);
                $mastheaddata->emptytopmiddle = false;
            }
            if (($PAGE->theme->settings->logoposition == 3)) {
                $mastheaddata->masthead_logotr = $mastheadlogo->export_for_template($this->output);
                $mastheaddata->emptytopright = false;
            }

            // MIDDLES 
            if (($PAGE->theme->settings->logoposition == 4)) {
                $mastheaddata->masthead_logoml = $mastheadlogo->export_for_template($this->output);
                $menuinline +=1;
                $mastheaddata->emptymiddleleft = false;
            }
            if (($PAGE->theme->settings->logoposition == 5)) {
                $mastheaddata->masthead_logomr = $mastheadlogo->export_for_template($this->output);
                $menuinline +=1;
                $mastheaddata->emptymiddleright = false;
            }

            // BOTTOMS
            if (($PAGE->theme->settings->logoposition == 6)) {
                $mastheaddata->masthead_logobl = $mastheadlogo->export_for_template($this->output);
                $mastheaddata->emptybottomleft = false;
            }
            if (($PAGE->theme->settings->logoposition == 7)) {
                $mastheaddata->masthead_logobc = $mastheadlogo->export_for_template($this->output);
                $mastheaddata->emptybottommiddle = false;
            }
            if (($PAGE->theme->settings->logoposition == 8)) {
                $mastheaddata->masthead_logobr = $mastheadlogo->export_for_template($this->output);
                $mastheaddata->emptybottomright = false;
            }

        }else{
            $mastheaddata->masthead_logo = $mastheadlogo->export_for_template($this->output);
        }

        // SEARCHPOSITION
        $mastheaddata->masthead_search = '';
        $mastheaddata->searchurl= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
        $mastheaddata->searchparameter = 'toolbarsearchtext';


            
        switch ($PAGE->theme->settings->searchsource) {
            case 0:
                $mastheaddata->searchurl= $CFG->wwwroot . '/totara/catalog/index.php';
                $mastheaddata->searchparameter = 'catalog_fts';
                break;
            case 1:
                $mastheaddata->searchurl= $CFG->wwwroot . '/course/search.php';
                $mastheaddata->searchparameter = 'search';
                break;
            case 2:
                $mastheaddata->searchurl= $CFG->wwwroot . '/search/index.php';
                $mastheaddata->searchparameter = 'q';
                break;
            case 3:
                $mastheaddata->searchurl= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
                $mastheaddata->searchparameter = 'toolbarsearchtext';
                break;
        }

        if (!empty($PAGE->theme->settings->searchposition)) {
            if (($PAGE->theme->settings->searchposition == 0)) {
                $mastheaddata->masthead_search = '';
            }

            // TOPS
            if (($PAGE->theme->settings->searchposition == 1)) {
                $mastheaddata->masthead_searchtl = $this->navsearch();
                $mastheaddata->emptytopleft = true;
            }
            if (($PAGE->theme->settings->searchposition == 2)) {
                $mastheaddata->masthead_searchtc = $this->navsearch();
                $mastheaddata->emptytopmiddle = false;
            }
            if (($PAGE->theme->settings->searchposition == 3)) {
                $mastheaddata->masthead_searchtr = $this->navsearch();
                $mastheaddata->emptytopright = false;                
            }

            // MIDDLES 
            if (($PAGE->theme->settings->searchposition == 4)) {
                $mastheaddata->masthead_searchml = $this->navsearch();
                $menuinline +=1;
                $mastheaddata->emptymiddleleft = false;
            }
            if (($PAGE->theme->settings->searchposition == 5)) {
                $mastheaddata->masthead_searchmr = $this->navsearch();
                $menuinline +=1;
                $mastheaddata->emptymiddleright = false;
            }

            // BOTTOMS
            if (($PAGE->theme->settings->searchposition == 6)) {
                $mastheaddata->masthead_searchbl = $this->navsearch();
                $mastheaddata->emptybottomleft = false;
            }
            if (($PAGE->theme->settings->searchposition == 7)) {
                $mastheaddata->masthead_searchbc = $this->navsearch();
                $mastheaddata->emptybottommiddle = false;
            }
            if (($PAGE->theme->settings->searchposition == 8)) {
                $mastheaddata->masthead_searchbr = $this->navsearch();
                $mastheaddata->emptybottomright = false;
            }

        }else{
            $mastheaddata->masthead_search = $this->navsearch();
        }

            // usermenuPOSITION
        $mastheaddata->masthead_usermenu = '';
        if (!empty($PAGE->theme->settings->usermenuposition)) {
            if (($PAGE->theme->settings->usermenuposition == 0)) {
                $mastheaddata->masthead_usermenu = $this->output->user_menu();
                $mastheaddata->masthead_usermenuhidden = $this->output->user_menu();
            }

            // TOPS
            if (($PAGE->theme->settings->usermenuposition == 1)) {
                $mastheaddata->masthead_usermenutl = $this->output->user_menu();
                $usermenupositionleft=true;
                $mastheaddata->emptytopleft = true;
            }
            if (($PAGE->theme->settings->usermenuposition == 2)) {
                $mastheaddata->masthead_usermenutc = $this->output->user_menu();
                $mastheaddata->emptytopmiddle = false;
            }
            if (($PAGE->theme->settings->usermenuposition == 3)) {
                $mastheaddata->masthead_usermenutr = $this->output->user_menu();
                $mastheaddata->emptytopright = false;
            }

            // MIDDLES 
            if (($PAGE->theme->settings->usermenuposition == 4)) {
                $mastheaddata->masthead_usermenuml = $this->output->user_menu();
                $usermenupositionleft=true;
                $menuinline +=1;
                $mastheaddata->emptymiddleleft = false;
            }
            if (($PAGE->theme->settings->usermenuposition == 5)) {
                $mastheaddata->masthead_usermenumr = $this->output->user_menu();
                $menuinline +=1;
                $mastheaddata->emptymiddleright = false;
            }

            // BOTTOMS
            if (($PAGE->theme->settings->usermenuposition == 6)) {
                $mastheaddata->masthead_usermenubl = $this->output->user_menu();
                $mastheaddata->emptybottomleft = false;
            }
            if (($PAGE->theme->settings->usermenuposition == 7)) {
                $mastheaddata->masthead_usermenubc = $this->output->user_menu();
                $mastheaddata->emptybottommiddle = false;
            }
            if (($PAGE->theme->settings->usermenuposition == 8)) {
                $mastheaddata->masthead_usermenubr = $this->output->user_menu();
                $mastheaddata->emptybottomright = false;
            }

        }else{
            $mastheaddata->masthead_usermenu = $this->output->user_menu();
            $mastheaddata->masthead_usermenuhidden = $this->output->user_menu();            
        }
        $mastheaddata->masthead_usermenu_custom_classes = '';
        if (isset($usermenupositionleft) && $usermenupositionleft) {
            $mastheaddata->masthead_usermenu_custom_classes = 'usermenuleft ';
        }

        // MENU POSITION
        $mastheaddata->masthead_menu = $mastheadmenudata;
        if (!empty($PAGE->theme->settings->menuposition)) {
            if (($PAGE->theme->settings->menuposition == 0)) {
                $mastheaddata->masthead_menu = '';
            }

            if (($PAGE->theme->settings->menuposition == 1)) {
                $mastheaddata->masthead_menul = $mastheadmenudata;
                $mastheaddata->emptymiddleleft = false;
            }
            if (($PAGE->theme->settings->menuposition == 2)) {
                $mastheaddata->masthead_menuc = $mastheadmenudata;
                $mastheaddata->emptymiddlemiddle = false;
            }
            if (($PAGE->theme->settings->menuposition == 3)) {
                $mastheaddata->masthead_menur = $mastheadmenudata;
                $mastheaddata->emptymiddleright = false;
            }
        }else{
            $mastheaddata->masthead_menu = $mastheadmenudata;
        }

        //Empty classes / ToDo
        $mastheaddata->masthead_usermenu_top_left_custom_classes = '';
        if ($mastheaddata->emptytopleft) {
            $mastheaddata->masthead_usermenu_top_left_custom_classes='empty';
        }

        $mastheaddata->masthead_usermenu_top_center_custom_classes = '';
        if ($mastheaddata->emptytopmiddle) {
            $mastheaddata->masthead_usermenu_top_center_custom_classes='empty';
        }
        $mastheaddata->masthead_usermenu_top_right_custom_classes = '';
        if ($mastheaddata->emptytopright) {
            $mastheaddata->masthead_usermenu_top_right_custom_classes='empty';
        }
        $mastheaddata->masthead_usermenu_middle_left_custom_classes = '';
        if ($mastheaddata->emptymiddleleft) {
            $mastheaddata->masthead_usermenu_middle_left_custom_classes='empty';
        }
        $mastheaddata->masthead_usermenu_middle_center_custom_classes = '';
        if ($mastheaddata->emptymiddlemiddle) {
            $mastheaddata->masthead_usermenu_middle_center_custom_classes='empty';
        }
        $mastheaddata->masthead_usermenu_middle_right_custom_classes = '';
        if ($mastheaddata->emptymiddleright) {
            $mastheaddata->masthead_usermenu_middle_right_custom_classes='empty';
        }
        $mastheaddata->masthead_usermenu_bottom_left_custom_classes = '';
        if ($mastheaddata->emptybottomleft) {
            $mastheaddata->masthead_usermenu_bottom_left_custom_classes='empty';
        }
        $mastheaddata->masthead_usermenu_bottom_center_custom_classes = '';
        if ($mastheaddata->emptybottommiddle) {
            $mastheaddata->masthead_usermenu_bottom_center_custom_classes='empty';
        }
        $mastheaddata->masthead_usermenu_bottom_right_custom_classes = '';
        if ($mastheaddata->emptybottomright) {
            $mastheaddata->masthead_usermenu_bottom_right_custom_classes='empty';
        }

        //Special case, recheck the whole logic with the Team in next versions...
        if (($PAGE->theme->settings->menuposition == 3)) {
            if ($mastheaddata->emptymiddlemiddle) {
                $mastheaddata->masthead_usermenu_middle_center_custom_classes='hidden';
                $mastheaddata->masthead_usermenu_middle_right_custom_classes='fb75';
            }
        }
        if (($PAGE->theme->settings->menuposition == 1)) {
            if ($mastheaddata->emptymiddlemiddle) {
                $mastheaddata->masthead_usermenu_middle_center_custom_classes='hidden';
                $mastheaddata->masthead_usermenu_middle_left_custom_classes='fb75';
            }
        }


        
        if (isset($menuinline) &&  $menuinline==1) {
            // $mastheaddata->masthead_menu_inline_class='fb50';
        }elseif (isset($menuinline) && $menuinline==0) {
            $mastheaddata->masthead_menu_inline_class='fb100';
        }

        $mastheaddata->masthead_plugins = $this->output->navbar_plugin_output();
        $mastheaddata->masthead_search = $this->output->search_box();
        // Even if we don't have a "navbar" we need this option, due to the poor design of the nonavbar option in the past.
        $mastheaddata->masthead_toggle = $this->output->navbar_button();
        $mastheaddata->masthead_usermenu = $this->output->user_menu();

        if (totara_core\quickaccessmenu\factory::can_current_user_have_quickaccessmenu()) {
            $menuinstance = totara_core\quickaccessmenu\factory::instance($USER->id);

            if (!empty($menuinstance->get_possible_items())) {
                $adminmenu = $menuinstance->get_menu();
                $quickaccessmenu = totara_core\output\quickaccessmenu::create_from_menu($adminmenu);
                $mastheaddata->masthead_quickaccessmenu = $quickaccessmenu->get_template_data();
            }
        }

        return $this->render_from_template('totara_core/masthead', $mastheaddata);
    }

    public function sidebar(bool $hasguestlangmenu = true, bool $nocustommenu = false) {
        global $CFG,$USER,$PAGE;

        if ($nocustommenu || !empty($this->page->layout_options['nototaramenu']) || !empty($this->page->layout_options['nocustommenu'])) {
            // No totara menu, or the old legacy no custom menu, in which case DO NOT generate the totara menu, its costly.
            $mastheadmenudata = new stdClass;
        } else {
            $menudata = totara_build_menu();
            $mastheadmenu = new totara_core\output\masthead_menu($menudata);
            $mastheadmenudata = $mastheadmenu->export_for_template($this->output);
        }

        $mastheaddata = new stdClass();
        $mastheaddata->masthead_lang = $hasguestlangmenu && (!isloggedin() || isguestuser()) ? $this->output->lang_menu() : '';

        // LOGOPOSITION
        
        $mastheaddata->masthead_menu = $mastheadmenudata;

        // $mastheaddata->masthead_plugins = $this->output->navbar_plugin_output();
        $mastheaddata->masthead_search = $this->output->search_box();
        // Even if we don't have a "navbar" we need this option, due to the poor design of the nonavbar option in the past.
        $mastheaddata->masthead_toggle = $this->output->navbar_button();
        $mastheaddata->masthead_usermenu = $this->output->user_menu();

        if (totara_core\quickaccessmenu\factory::can_current_user_have_quickaccessmenu()) {
            $menuinstance = totara_core\quickaccessmenu\factory::instance($USER->id);

            if (!empty($menuinstance->get_possible_items())) {
                $adminmenu = $menuinstance->get_menu();
                $quickaccessmenu = totara_core\output\quickaccessmenu::create_from_menu($adminmenu);
                $mastheaddata->masthead_quickaccessmenu = $quickaccessmenu->get_template_data();
            }
        }

        $mastheaddata->masthead_search = '';
        $mastheaddata->searchurl= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
        $mastheaddata->searchparameter = 'toolbarsearchtext';


            
        switch ($PAGE->theme->settings->searchsource) {
            case 0:
                $mastheaddata->searchurl= $CFG->wwwroot . '/totara/catalog/index.php';
                $mastheaddata->searchparameter = 'catalog_fts';
                break;
            case 1:
                $mastheaddata->searchurl= $CFG->wwwroot . '/course/search.php';
                $mastheaddata->searchparameter = 'search';
                break;
            case 2:
                $mastheaddata->searchurl= $CFG->wwwroot . '/search/index.php';
                $mastheaddata->searchparameter = 'q';
                break;
            case 3:
                $mastheaddata->searchurl= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
                $mastheaddata->searchparameter = 'toolbarsearchtext';
                break;
        }
        
        if (!empty($PAGE->theme->settings->searchposition)) {
            if (($PAGE->theme->settings->searchposition == 9)) {
                $mastheaddata->masthead_searchsb = $this->navsearch();
            }
        }
        return $this->render_from_template('theme_ethink/sidebar', $mastheaddata);
    }
    public function navsearch() {
        global $CFG,$PAGE;


        $searchdata = new stdClass();
        $searchdata->searchurl= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
        $searchdata->searchparameter = 'toolbarsearchtext';


            
        switch ($PAGE->theme->settings->searchsource) {
            case 0:
                $searchdata->searchurl= $CFG->wwwroot . '/totara/catalog/index.php';
                $searchdata->searchparameter = 'catalog_fts';
                break;
            case 1:
                $searchdata->searchurl= $CFG->wwwroot . '/course/search.php';
                $searchdata->searchparameter = 'search';
                break;
            case 2:
                $searchdata->searchurl= $CFG->wwwroot . '/search/index.php';
                $searchdata->searchparameter = 'q';
                break;
            case 3:
                $searchdata->searchurl= $CFG->wwwroot . '/totara/coursecatalog/courses.php';
                $searchdata->searchparameter = 'toolbarsearchtext';
                break;
        }
        return $this->render_from_template('theme_ethink/nav_search', $searchdata);
    }

}
