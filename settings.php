<?php
/*
 * This file is part of Totara LMS
 *
 * Copyright (C) 2015 onwards Totara Learning Solutions LTD
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Brian Barnes <brian.barnes@totaralearning.com>
 * @author Joby Harding <joby.harding@totaralearning.com>
 * @package theme_ethink
 */

defined('MOODLE_INTERNAL') || die;

use theme_ethink\css_processor;

global $PAGE;

$component = 'theme_ethink';

$settings = null;

// Create own category and define pages.
$ADMIN->add('themes', new admin_category($component, 'Ethink'));

require($CFG->dirroot.'/theme/ethink/settings/general.php');
require($CFG->dirroot.'/theme/ethink/settings/header.php');
require($CFG->dirroot.'/theme/ethink/settings/generalregions.php');
require($CFG->dirroot.'/theme/ethink/settings/fpregions.php');
require($CFG->dirroot.'/theme/ethink/settings/dashboardregions.php');
require($CFG->dirroot.'/theme/ethink/settings/search.php');
require($CFG->dirroot.'/theme/ethink/settings/slideshow.php');
require($CFG->dirroot.'/theme/ethink/settings/login.php');
require($CFG->dirroot.'/theme/ethink/settings/socialicons.php');
require($CFG->dirroot.'/theme/ethink/settings/tiles.php');
require($CFG->dirroot.'/theme/ethink/settings/fonts.php');
require($CFG->dirroot.'/theme/ethink/settings/footer.php');


// require($CFG->dirroot.'/theme/ethink/settings/socialicons.php');

// require($CFG->dirroot.'/theme/ethink/settings/footer.php');
// require($CFG->dirroot.'/theme/ethink/settings/footer.php');
